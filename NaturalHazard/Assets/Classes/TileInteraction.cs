﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public static class TileInteraction
{
    private class Spawn
    {
        public float time;
        public TileType disasterType;

        public bool IsDue
        {
            get { return currentTime >= time; }
        }

        public Spawn(float time, TileType disasterType)
        {
            this.time = time;
            this.disasterType = disasterType;
        }
    }

    internal static List<Tile> DataBuffer = new List<Tile>();
    private static float currentTime;
    private static Dictionary<Vector2, Spawn> spawns = new Dictionary<Vector2, Spawn>();

    internal static void CalculateTileTemperature()
    {
        foreach (Tile computed in DataBuffer)
        {
            float partialSum = 0, weights = 0;
            for (int y = -2; y <= 2; y++)
            {
                int positionY = (int)computed.GridPosition.y + y;
                if (positionY >= 0 && positionY < Map.sizeY)
                {
                    for (int x = -2; x <= 2; x++)
                    {
                        int positionX = (int)computed.GridPosition.x + x;
                        if (positionX >= 0 && positionX < Map.sizeX)
                        {
                            Tile t = DataBuffer[Map.GetMapIndex(positionX, positionY)];
                            float weight = 1f / Mathf.Pow(Map.TileDistance(computed.GridPosition, t.GridPosition) + 1, 2);
                            partialSum += t.BaseTemperature * weight;
                            weights += weight;
                        }
                    }
                }
            }
            computed.Temperature = Mathf.Clamp(partialSum / weights, -50f, 50f);
        }
    }

    internal static void CalculateHumidityFlow()
    {
        List<Tile> data = DataBuffer.OrderBy(x => x.Humidity).ToList();
        for (int i = 0; i < data.Count(); i++)
        {
            Tile dryTile = data[i];
            Vector2 position = dryTile.GridPosition;
            List<Tile> localSources = new List<Tile>();
            if (position.x > 0)
            {
                Tile t = DataBuffer.ElementAt(Map.GetMapIndex(position.x - 1, position.y));
                if (dryTile.Humidity < t.Humidity)
                    localSources.Add(t);
            }
            if (position.x < Map.sizeX - 1)
            {
                Tile t = DataBuffer.ElementAt(Map.GetMapIndex(position.x + 1, position.y));
                if (dryTile.Humidity < t.Humidity)
                    localSources.Add(t);
            }
            if (position.y > 0)
            {
                Tile t = DataBuffer.ElementAt(Map.GetMapIndex(position.x, position.y - 1));
                if (dryTile.Humidity < t.Humidity)
                    localSources.Add(t);
            }
            if (position.y < Map.sizeY - 1)
            {
                Tile t = DataBuffer.ElementAt(Map.GetMapIndex(position.x, position.y + 1));
                if (dryTile.Humidity < t.Humidity)
                    localSources.Add(t);
            }
            localSources = localSources.OrderByDescending(x => x.Humidity).ToList();
            foreach (Tile source in localSources)
            {
                if (dryTile.Humidity < source.Humidity)
                {
                    float humidityChange = (source.Humidity - dryTile.Humidity) / dryTile.HumidityIngestSuppression;
                    dryTile.Humidity += humidityChange;
                    source.Humidity -= humidityChange;
                }
            }
        }
    }

    internal static void CalculateHumidityGeneration()
    {
        foreach (Tile t in DataBuffer)
        {
            t.Humidity = Mathf.Clamp01(t.Humidity + t.HumidityGeneration);
        }
    }

    internal static float CalculateManaGeneration()
    {
        return DataBuffer.Where(x => x.Owner == GameEntity.Player).Sum(x => x.ManaGeneration);
    }

    internal static void EvaluateScenario(Scenario s, float currentTime)
    {
        TileInteraction.currentTime = currentTime;
        if (!s.DisasterSpawned && s.SpawningTime + s.GameStartTime <= currentTime)
        {
            foreach (Point tileIndex in s.SpawningTilesIndices)
            {
                Tile t = DataBuffer[Map.GetMapIndex(tileIndex.X, tileIndex.Y)];
                t.TileType = s.DisasterType;
                t.ChangeTimestamp = currentTime;
                t.Owner = GameEntity.Disaster;
            }
            s.DisasterSpawned = true;
        }
        else
        {
            if (s.DisasterSpawned && DataBuffer.Count(x => x.IsDisaster()) == 0 && DataBuffer.Count(x => x.TileType == TileType.City) == s.CityTilesCount)
                s.ScenarioState = ScenarioState.CityUndamaged;
            else if (s.DisasterSpawned && DataBuffer.Count(x => x.IsDisaster()) == 0 && DataBuffer.Count(x => x.TileType == TileType.City) < s.CityTilesCount)
                s.ScenarioState = ScenarioState.CityDamaged;
            else if (s.DisasterSpawned && DataBuffer.Count(x => x.TileType == TileType.City) == 0)
                s.ScenarioState = ScenarioState.CityDestroyed;
        }
    }

    internal static void DisasterControl()
    {
        foreach (Tile disasterTile in DataBuffer.Where(x => x.IsDisaster()))
        {
            switch (disasterTile.TileType)
            {
                case TileType.Fire: FireBehavior(disasterTile); break;
                case TileType.Desert: DesertBehavior(disasterTile); break;
                case TileType.Blizzard: BlizzardBehavior(disasterTile); break;
                case TileType.Swamp: SwampBehavior(disasterTile); break;
            }
        }
        SpreadDisasters();
    }

    private static void SpreadDisasters()
    {
        for (int i = 0; i < spawns.Count; i++)
        {
            var item = spawns.ElementAt(i);
            if (item.Value.IsDue)
            {
                Tile x = DataBuffer[Map.GetMapIndex(item.Key.x, item.Key.y)];
                x.TileType = item.Value.disasterType;
                x.ChangeTimestamp = currentTime;
                x.Owner = GameEntity.Disaster;
                spawns.Remove(item.Key);
                i--;
            }
        }
    }

    private static void CalculateSpread(Tile neighbourTile, Tile disasterTile, float liveExpectancy, Func<Tile, float> spawnDelay)
    {
        bool hasSpawnSet = spawns.ContainsKey(neighbourTile.GridPosition);
        if (!neighbourTile.IsDevastated() && !neighbourTile.IsDisaster())
        {
            bool threshold = currentTime >= spawnDelay(neighbourTile);

            if (!hasSpawnSet && threshold)
            {
                spawns.Add(neighbourTile.GridPosition,
                    new Spawn(
                        currentTime + TriangleRng(disasterTile.ChangeTimestamp + liveExpectancy - currentTime),
                        disasterTile.TileType));
            }
            else if (hasSpawnSet && !threshold)
                spawns.Remove(neighbourTile.GridPosition);
        }
    }

    private static float TriangleRng(float maxValue)
    {
        var first = System.Convert.ToSingle(GameManager.rng.NextDouble()) * maxValue;
        var second = System.Convert.ToSingle(GameManager.rng.NextDouble()) * maxValue;
        return (first + second) / 2f;
    }

    private static void FireBehavior(Tile t)
    {
        float liveExpectancy = Globals.FireExpectedLife - (Mathf.Abs(t.Temperature) / 50f) * Globals.FireTempSensitivity - t.Humidity * Globals.FireHumiditySensitivity;
        if (currentTime >= t.ChangeTimestamp + liveExpectancy)
        {
            t.TileType = TileType.WitheredLand;
            t.ChangeTimestamp = currentTime;
            t.Owner = GameEntity.Devastation;
        }
        else
        {
            Func<Tile, float> threshold = (x) => t.ChangeTimestamp
                + Globals.FireNewSpawnDelay
                - (x.Temperature / 50f) * Globals.FireTempSensitivity * Globals.FireSpawnSensitivityRatio
                + x.Humidity * Globals.FireHumiditySensitivity * Globals.FireSpawnSensitivityRatio;
            if (t.GridPosition.y < Map.sizeY - 1)
            {
                Tile north = DataBuffer[Map.GetMapIndex(t.GridPosition.x, t.GridPosition.y + 1)];
                CalculateSpread(north, t, liveExpectancy, threshold);
            }
            if (t.GridPosition.y > 0)
            {
                Tile south = DataBuffer[Map.GetMapIndex(t.GridPosition.x, t.GridPosition.y - 1)];
                CalculateSpread(south, t, liveExpectancy, threshold);
            }
            if (t.GridPosition.x < Map.sizeX - 1)
            {
                Tile west = DataBuffer[Map.GetMapIndex(t.GridPosition.x + 1, t.GridPosition.y)];
                CalculateSpread(west, t, liveExpectancy, threshold);
            }
            if (t.GridPosition.x > 0)
            {
                Tile east = DataBuffer[Map.GetMapIndex(t.GridPosition.x - 1, t.GridPosition.y)];
                CalculateSpread(east, t, liveExpectancy, threshold);
            }
        }
    }

    private static void DesertBehavior(Tile t)
    {
        float liveExpectancy = Globals.DesertExpectedLife + (t.Temperature / 50) * Globals.DesertTempSensitivity - t.Humidity * Globals.DesertHumiditySensitivity;
        if (currentTime >= t.ChangeTimestamp + liveExpectancy && t.Humidity < 0.5f)
        {
            t.TileType = TileType.Desert;
            t.ChangeTimestamp = currentTime;
            t.Owner = GameEntity.Disaster;
        }
        else if (currentTime >= t.ChangeTimestamp + liveExpectancy && t.Humidity >= 0.5f)
        {
            t.TileType = TileType.Plain;
            t.ChangeTimestamp = currentTime;
            t.Owner = GameEntity.Nature;
        }
        else
        {
            Func<Tile, float> threshold = (x) => t.ChangeTimestamp
                + Globals.DesertNewSpawnDelay
                - (x.Temperature / 50f) * Globals.DesertTempSensitivity * Globals.DesertSpawnSensitivityRatio
                + x.Humidity * Globals.DesertHumiditySensitivity * Globals.DesertSpawnSensitivityRatio;
            if (t.GridPosition.y < Map.sizeY - 1)
            {
                Tile north = DataBuffer[Map.GetMapIndex(t.GridPosition.x, t.GridPosition.y + 1)];
                CalculateSpread(north, t, liveExpectancy, threshold);
            }
            if (t.GridPosition.y > 0)
            {
                Tile south = DataBuffer[Map.GetMapIndex(t.GridPosition.x, t.GridPosition.y - 1)];
                CalculateSpread(south, t, liveExpectancy, threshold);
            }
            if (t.GridPosition.x < Map.sizeX - 1)
            {
                Tile west = DataBuffer[Map.GetMapIndex(t.GridPosition.x + 1, t.GridPosition.y)];
                CalculateSpread(west, t, liveExpectancy, threshold);
            }
            if (t.GridPosition.x > 0)
            {
                Tile east = DataBuffer[Map.GetMapIndex(t.GridPosition.x - 1, t.GridPosition.y)];
                CalculateSpread(east, t, liveExpectancy, threshold);
            }
        }
    }

    private static void BlizzardBehavior(Tile t)
    {
        float liveExpectancy = Globals.BlizzardExpectedLife - (t.Temperature / 50f) * Globals.BlizzardTempSensitivity - (1f - t.Humidity) * Globals.BlizzardHumiditySensitivity;
        if (currentTime >= t.ChangeTimestamp + liveExpectancy)
        {
            t.TileType = TileType.FrozenLand;
            t.ChangeTimestamp = currentTime;
            t.Owner = GameEntity.Devastation;
        }
        else
        {
            Func<Tile, float> threshold = (x) => t.ChangeTimestamp
                + Globals.BlizzardNewSpawnDelay
                + (x.Temperature / 50f) * Globals.BlizzardTempSensitivity * Globals.BlizzardSpawnSensitivityRatio
                + (1f - x.Humidity) * Globals.BlizzardHumiditySensitivity * Globals.BlizzardSpawnSensitivityRatio;
            if (t.GridPosition.y < Map.sizeY - 1)
            {
                Tile north = DataBuffer[Map.GetMapIndex(t.GridPosition.x, t.GridPosition.y + 1)];
                CalculateSpread(north, t, liveExpectancy, threshold);
            }
            if (t.GridPosition.y > 0)
            {
                Tile south = DataBuffer[Map.GetMapIndex(t.GridPosition.x, t.GridPosition.y - 1)];
                CalculateSpread(south, t, liveExpectancy, threshold);
            }
            if (t.GridPosition.x < Map.sizeX - 1)
            {
                Tile west = DataBuffer[Map.GetMapIndex(t.GridPosition.x + 1, t.GridPosition.y)];
                CalculateSpread(west, t, liveExpectancy, threshold);
            }
            if (t.GridPosition.x > 0)
            {
                Tile east = DataBuffer[Map.GetMapIndex(t.GridPosition.x - 1, t.GridPosition.y)];
                CalculateSpread(east, t, liveExpectancy, threshold);
            }
        }
    }

    private static void SwampBehavior(Tile t)
    {
        float liveExpectancy = Globals.SwampExpectedLife + (t.Temperature / 50f) * Globals.SwampTempSensitivity - (1f - t.Humidity) * Globals.SwampHumiditySensitivity;
        if (currentTime >= t.ChangeTimestamp + liveExpectancy && t.Humidity > 0.5f)
        {
            t.TileType = TileType.Swamp;
            t.ChangeTimestamp = currentTime;
            t.Owner = GameEntity.Disaster;
        }
        else if (currentTime >= t.ChangeTimestamp + liveExpectancy && t.Humidity <= 0.5f)
        {
            t.TileType = TileType.Plain;
            t.ChangeTimestamp = currentTime;
            t.Owner = GameEntity.Nature;
        }
        else
        {
            Func<Tile, float> threshold = (x) => t.ChangeTimestamp
                + Globals.SwampNewSpawnDelay
                - (x.Temperature / 50f) * Globals.SwampTempSensitivity * Globals.SwampSpawnSensitivityRatio
                + (1f - x.Humidity) * Globals.SwampHumiditySensitivity * Globals.SwampSpawnSensitivityRatio;
            if (t.GridPosition.y < Map.sizeY - 1)
            {
                Tile north = DataBuffer[Map.GetMapIndex(t.GridPosition.x, t.GridPosition.y + 1)];
                CalculateSpread(north, t, liveExpectancy, threshold);
            }
            if (t.GridPosition.y > 0)
            {
                Tile south = DataBuffer[Map.GetMapIndex(t.GridPosition.x, t.GridPosition.y - 1)];
                CalculateSpread(south, t, liveExpectancy, threshold);
            }
            if (t.GridPosition.x < Map.sizeX - 1)
            {
                Tile west = DataBuffer[Map.GetMapIndex(t.GridPosition.x + 1, t.GridPosition.y)];
                CalculateSpread(west, t, liveExpectancy, threshold);
            }
            if (t.GridPosition.x > 0)
            {
                Tile east = DataBuffer[Map.GetMapIndex(t.GridPosition.x - 1, t.GridPosition.y)];
                CalculateSpread(east, t, liveExpectancy, threshold);
            }
        }
    }

    public static void Reset()
    {
        DataBuffer.Clear();
        spawns = new Dictionary<Vector2, Spawn>();
    }

}
