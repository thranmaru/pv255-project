﻿using System.Collections;
using UnityEngine;

[System.Flags]
public enum TileType
{
    None = 0, City = 1, Forest = 2, Plain = 4, River = 8, Lake = 16, Fire = 32, Desert = 64, Blizzard = 128, Swamp = 256, WitheredLand = 512, FrozenLand = 1024
}

[System.Flags]
public enum GameEntity
{
    Nobody = 0, Nature = 1, Player = 2, Disaster = 4, Devastation = 5
}

public class Tile
{
    private bool tileOwnedByPlayer = false;

    public Vector2 GridPosition
    {
        get;
        set;
    }

    public float Temperature
    {
        get;
        set;
    }

    public float Humidity
    {
        get;
        set;
    }

    public int AltitudeNW
    {
        get;
        set;
    }

    public int AltitudeNE
    {
        get;
        set;
    }

    public int AltitudeSW
    {
        get;
        set;
    }

    public int AltitudeSE
    {
        get;
        set;
    }

    public int Altitude
    {
        get;
        set;
    }

    public TileType TileType
    {
        get;
        set;
    }

    public float ChangeTimestamp
    {
        get;
        set;
    }

    public GameEntity Owner
    {
        get
        {
            if (tileOwnedByPlayer)
                return GameEntity.Player;
            else if (TileType.IsDisaster())
                return GameEntity.Disaster;
            else if (TileType.IsNature())
                return GameEntity.Nature;
            else if (TileType.IsDevastated())
                return GameEntity.Devastation;
            else
                return GameEntity.Nobody;
        }
        set
        {
            if (value == GameEntity.Player)
                tileOwnedByPlayer = true;
            else
                tileOwnedByPlayer = false;
        }
    }

    public float BaseTemperature
    {
        get
        {
            switch (TileType)
            {
                case TileType.Fire:
                    return Globals.TempClamp(Altitude * -10f + 50f);
                case TileType.Desert:
                    return Globals.TempClamp(Altitude * -10f + 40f);
                case TileType.Blizzard:
                    return Globals.TempClamp(Altitude * -10f - 20f);
                case TileType.Swamp:
                    return Globals.TempClamp(Altitude * -10f + 20f);
                default:
                    return Globals.TempClamp(Altitude * -10f + 10f);
            }
        }
    }

    public float HumidityIngestSuppression
    {
        get
        {
            switch (TileType)
            {
                case TileType.City:
                    return 20f;
                case TileType.Forest:
                    return 10f;
                case TileType.Plain:
                    return 2f;
                case TileType.River:
                    return 3f;
                case TileType.Lake:
                    return 2f;
                case TileType.Fire:
                    return 5f;
                case TileType.Desert:
                    return 2f;
                case TileType.Blizzard:
                    return 2f;
                case TileType.Swamp:
                    return 5f;
                case TileType.WitheredLand:
                    return 50f;
                case TileType.FrozenLand:
                    return 50f;
                default:
                    return float.MaxValue;
            }
        }
    }

    public float HumidityGeneration
    {
        get
        {
            switch (TileType)
            {
                case TileType.City:
                    return -0.002f;
                case TileType.Forest:
                    return -0.002f;
                case TileType.Plain:
                    return -0.0005f;
                case TileType.River:
                    return 0.01f;
                case TileType.Lake:
                    return 0.02f;
                case TileType.Fire:
                    return -0.015f;
                case TileType.Desert:
                    return -0.015f;
                case TileType.Blizzard:
                    return 0.008f;
                case TileType.Swamp:
                    return 0.004f;
                case TileType.WitheredLand:
                    return -0.0001f;
                case TileType.FrozenLand:
                    return 0f;
                default:
                    return 0f;
            }
        }
    }

    public float HumidityToManaRatio
    {
        get
        {
            switch (TileType)
            {
                case TileType.Forest:
                    return 0.2f;
                case TileType.Plain:
                    return 0.02f;
                case TileType.River:
                    return 0.01f;
                case TileType.Lake:
                    return 0.02f;
                default:
                    return 0f;
            }
        }
    }

    public float ManaGeneration
    {
        get
        {
            return Humidity * HumidityToManaRatio;
        }
    }


    public Tile()
    {
        GridPosition = Vector2.zero;
    }

    public static Tile Copy(Tile t)
    {
        Tile copy = new Tile();
        copy.GridPosition = t.GridPosition;
        copy.Altitude = t.Altitude;
        copy.AltitudeNW = t.AltitudeNW;
        copy.AltitudeNE = t.AltitudeNE;
        copy.AltitudeSW = t.AltitudeSW;
        copy.AltitudeSE = t.AltitudeSE;
        copy.Humidity = t.Humidity;
        copy.Temperature = t.Temperature;
        copy.TileType = t.TileType;
        copy.ChangeTimestamp = t.ChangeTimestamp;
        copy.Owner = t.Owner;
        return copy;
    }

    public override string ToString()
    {
        return TileType.ToString();
    }
}