﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public enum ScenarioState
{
    Playing, CityDestroyed, CityDamaged, CityUndamaged
}

public class Scenario
{
    public List<Point> SpawningTilesIndices { get; set; }
    public float GameStartTime { get; set; }
    public float SpawningTime { get; set; }
    public int CityTilesCount { get; set; }
    public TileType DisasterType { get; set; }
    public ScenarioState ScenarioState { get; set; }
    public int StartingMana { get; set; }

    public const float DefaultSpawnTime = 10f;
    public const int DefaultStartingMana = 500;

    public bool DisasterSpawned { get; set; }

    public Scenario()
    {
        SpawningTilesIndices = new List<Point>();
        DisasterType = TileType.None;
        SpawningTime = DefaultSpawnTime;
        DisasterSpawned = false;
        StartingMana = DefaultStartingMana;
    }

    public Scenario(List<Point> spawningTilesIndices, float gameStartTime, float spawningTime, TileType disasterType, int cityTilesCount, int startingMana)
    {
        SpawningTilesIndices = spawningTilesIndices;
        GameStartTime = gameStartTime;
        SpawningTime = spawningTime;
        if (!disasterType.IsDisaster())
            Debug.LogException(new System.ArgumentException("Invalid TileType for disaster", "disasterType"));
        DisasterType = disasterType;
        CityTilesCount = cityTilesCount;
        StartingMana = startingMana;
        ScenarioState = ScenarioState.Playing;
        DisasterSpawned = false;
    }
}
