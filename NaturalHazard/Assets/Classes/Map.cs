﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using UnityEngine;
using Object = UnityEngine.Object;


public class Map
{
    public static int sizeX = 30;
    public static int sizeY = 30;

    public List<TileScript> Tiles = new List<TileScript>();
    public Scenario Scenario;

    private MapView mapView = MapView.Normal;

    private readonly Transform gameField;
    private List<Tile> oldBuffer = new List<Tile>();

    public int SizeX
    {
        get { return sizeX; }
        set
        {
            if (sizeX != value)
            {
                sizeX = value;
                LoadBlankMap();
            }
        }
    }

    public int SizeY
    {
        get { return sizeY; }
        set
        {
            if (sizeY != value)
            {
                sizeY = value;
                LoadBlankMap();
            }
        }
    }

    public Map(Transform gameField)
    {
        this.gameField = gameField;
    }

    public void LoadMapFromFile(string path)
    {
#if !UNITY_WEBPLAYER
        LoadMapFromText(System.IO.File.ReadAllText(path, Encoding.UTF8));
#endif
    }

    public void LoadMapFromResource(string mapName)
    {
        LoadMapFromText(Resources.Load<TextAsset>("Maps/" + mapName).text);
    }

    private void LoadMapFromText(string text)
    {
        ClearMap();
        foreach (string line in text.Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.RemoveEmptyEntries))
        {
            string[] parts = line.Split(':');
            string[] values;
            if (parts.Length < 2)
                return;
            switch (parts[0])
            {
                case "map":
                    values = parts[1].Split(';');
                    sizeX = int.Parse(values[0]);
                    sizeY = int.Parse(values[1]);
                    break;
                case "tile":
                    values = parts[1].Split(';');
                    AddTile(int.Parse(values[0]), int.Parse(values[1]), int.Parse(values[2]), int.Parse(values[3]), int.Parse(values[4]), int.Parse(values[5]), (TileType)int.Parse(values[6]), (GameEntity)int.Parse(values[7]));
                    break;
                case "scenario":
                    values = parts[1].Split(';');
                    Scenario = new Scenario(GetSpawnPositions(values[0]), 0f, float.Parse(values[1]), (TileType)int.Parse(values[2]), int.Parse(values[3]), int.Parse(values[4]));
                    break;
            }
        }

        foreach (TileScript tile in Tiles)
        {
            if (IsRiverOrLake(tile))
                UpdateWaterContinuity(tile);
            else
                UpdateTerrainContinuity(tile);
        }
    }

    public void LoadBlankMap()
    {
        ClearMap();

        for (int i = 0; i < sizeX; i++)
        {
            for (int j = 0; j < sizeY; j++)
            {
                AddTile(j, i, 0, 0, 0, 0, TileType.Plain, GameEntity.Nature);
            }
        }

        Scenario = new Scenario();
    }

    public void SaveMap(string path)
    {
#if !UNITY_WEBPLAYER
        StringBuilder fileText = new StringBuilder();
        fileText.AppendFormat("map:{0};{1}", sizeX, sizeY);
        fileText.AppendLine();
        foreach (TileScript t in Tiles)
        {
            Tile td = t.TileData;
            fileText.AppendFormat("tile:{0};{1};{2};{3};{4};{5};{6};{7}", td.GridPosition.x, td.GridPosition.y, td.AltitudeNE, td.AltitudeNW, td.AltitudeSE, td.AltitudeSW, (int)td.TileType, (int)td.Owner);
            fileText.AppendLine();
        }
        fileText.AppendFormat("scenario:{0};{1};{2};{3};{4}", GetSpawnPositions(Scenario.SpawningTilesIndices), Scenario.SpawningTime, (int)Scenario.DisasterType, Tiles.Count(tile => tile.TileData.TileType == TileType.City), Scenario.StartingMana);
        System.IO.File.WriteAllText(path, fileText.ToString(), Encoding.UTF8);
#endif
    }

    private List<Point> GetSpawnPositions(string p)
    {
        string[] points = p.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
        List<Point> list = new List<Point>();
        foreach (string point in points)
        {
            string[] coords = point.Split('|');
            list.Add(new Point(int.Parse(coords[0]), int.Parse(coords[1])));
        }
        return list;
    }

    private string GetSpawnPositions(List<Point> indices)
    {
        if (indices.Count == 0)
            return string.Empty;
        string text = string.Format("{0}|{1}", indices[0].X, indices[0].Y);
        for (int i = 1; i < indices.Count; i++)
        {
            text += string.Format(",{0}|{1}", indices[i].X, indices[i].Y);
        }
        return text;
    }

    public void ChangeMapView(MapView view)
    {
        mapView = view;

        foreach (TileScript t in Tiles)
        {
            t.RenderTerrain = true;
            if (view == MapView.Normal && t.TileData.Owner != GameEntity.Player)
                t.ApplyShader("Custom/GreyScale");
            else
                t.ChangeView(mapView);
        }
    }

    public void UpdateMapView()
    {
        ChangeMapView(mapView);
    }

    public void UpdateMapViewValues()
    {
        foreach (TileScript t in Tiles)
        {
            t.UpdateMapViewValues(mapView);
        }
    }

    public void CopyAndSwapTileData(TileScript newTileScript, int index)
    {
        newTileScript.transform.position = Tiles[index].transform.position;
        newTileScript.TileData = Tiles[index].TileData;
        newTileScript.TerrainObjects = Tiles[index].TerrainObjects;
        foreach (var terrainObj in Tiles[index].TerrainObjects)
        {
            terrainObj.transform.SetParent(newTileScript.transform, true/* don't react to rotation changes */);
        }
        Tiles[index] = newTileScript;
        newTileScript.transform.parent = gameField;
        newTileScript.ChangeView(mapView);
    }

    public void ChangeTileType(TileScript tile, TileType type)
    {
        if (tile.TileData.TileType == type)
            return;

        tile.TileData.TileType = type;
        tile.SetupTerrainObjs();
        UpdateWaterContinuity(tile);
    }

    public bool MovePoint(float xValue, float zValue, int delta)
    {
        bool result = false;

        // Move SW
        int x = Convert.ToInt32(xValue + 5) / 10;
        int y = Convert.ToInt32(zValue + 5) / 10;
        TileScript t = GetTile(x, y);
        if (t && t.TileData.AltitudeSW + delta <= TileScript.MaxAltitude && t.TileData.AltitudeSW + delta >= TileScript.MinAltitude)
        {
            t.MovePointSW(delta);
            result = true;
        }

        // Move NE
        x = Convert.ToInt32(xValue - 5) / 10;
        y = Convert.ToInt32(zValue - 5) / 10;
        t = GetTile(x, y);
        if (t && t.TileData.AltitudeNE + delta <= TileScript.MaxAltitude && t.TileData.AltitudeNE + delta >= TileScript.MinAltitude)
        {
            t.MovePointNE(delta);
            result = true;
        }

        // Move SE
        x = Convert.ToInt32(xValue - 5) / 10;
        y = Convert.ToInt32(zValue + 5) / 10;
        t = GetTile(x, y);
        if (t && t.TileData.AltitudeSE + delta <= TileScript.MaxAltitude && t.TileData.AltitudeSE + delta >= TileScript.MinAltitude)
        {
            t.MovePointSE(delta);
            result = true;
        }

        // Move NW
        x = Convert.ToInt32(xValue + 5) / 10;
        y = Convert.ToInt32(zValue - 5) / 10;
        t = GetTile(x, y);
        if (t && t.TileData.AltitudeNW + delta <= TileScript.MaxAltitude && t.TileData.AltitudeNW + delta >= TileScript.MinAltitude)
        {
            t.MovePointNW(delta);
            result = true;
        }
        return result;
    }

    public bool MovePointWithIndex(int x, int y, int delta)
    {
        return MovePoint((x * 10) - 5, (y * 10) - 5, delta);
    }

    /// <summary>Returns number of moved tiles</summary>
    public int MovePoints(List<Point> pointIndices, int delta, bool forceMove, float availableMana = 0f)
    {
        int movedPointsCount = 0;
        var originalAltitudes = ComputeAltitudeMap();

        var movedTiles = new HashSet<TileScript>();
        Action<int, int> addTilesToMovedAroundCoords = (x, y) =>
        {
            var tmpTile = GetTile(x - 1, y - 1);
            if (x - 1 >= 0 && y - 1 >= 0 && tmpTile != null)
                movedTiles.Add(tmpTile);
            tmpTile = GetTile(x - 1, y);
            if (x - 1 >= 0 && tmpTile != null)
                movedTiles.Add(tmpTile);
            tmpTile = GetTile(x, y - 1);
            if (y - 1 >= 0 && tmpTile != null)
                movedTiles.Add(tmpTile);
            tmpTile = GetTile(x, y);
            if (tmpTile != null)
                movedTiles.Add(tmpTile);
        };

        foreach (Point p in pointIndices)
        {
            if (MovePointWithIndex(p.X, p.Y, delta))
            {
                movedPointsCount++;
                addTilesToMovedAroundCoords(p.X, p.Y);
            }
        }

        // Make a copy to work with
        List<Point> movedPointsIndices = new List<Point>(pointIndices);

        bool change = true;
        while (change)
        {
            change = false;
            var tmpIndices = new List<Point>(movedPointsIndices);

            foreach (var point in movedPointsIndices)
            {
                int x = point.X;
                int y = point.Y;

                var altitudes = ComputeAltitudeMap();

                // Check for change by 2 to the west
                if (x - 1 >= 0 && altitudes[x, y] == altitudes[x - 1, y] + 2 * delta)
                {
                    if (MovePointWithIndex(x - 1, y, delta))
                    {
                        altitudes[x - 1, y] += delta;
                        tmpIndices.Add(new Point(x - 1, y));
                        movedPointsCount++;
                        addTilesToMovedAroundCoords(x - 1, y);
                        change = true;
                    }
                }
                // east
                else if (x + 1 <= sizeX && altitudes[x, y] == altitudes[x + 1, y] + 2 * delta)
                {
                    if (MovePointWithIndex(x + 1, y, delta))
                    {
                        altitudes[x + 1, y] += delta;
                        tmpIndices.Add(new Point(x + 1, y));
                        movedPointsCount++;
                        addTilesToMovedAroundCoords(x + 1, y);
                        change = true;
                    }
                }
                // south
                else if (y - 1 >= 0 && altitudes[x, y] == altitudes[x, y - 1] + 2 * delta)
                {
                    if (MovePointWithIndex(x, y - 1, delta))
                    {
                        altitudes[x, y - 1] += delta;
                        tmpIndices.Add(new Point(x, y - 1));
                        movedPointsCount++;
                        addTilesToMovedAroundCoords(x, y - 1);
                        change = true;
                    }
                }
                // north
                else if (y + 1 <= sizeY && altitudes[x, y] == altitudes[x, y + 1] + 2 * delta)
                {
                    if (MovePointWithIndex(x, y + 1, delta))
                    {
                        altitudes[x, y + 1] += delta;
                        tmpIndices.Add(new Point(x, y + 1));
                        movedPointsCount++;
                        addTilesToMovedAroundCoords(x, y + 1);
                        change = true;
                    }
                }
            }

            movedPointsIndices = tmpIndices;
        }

        bool enoughMana = availableMana >= movedPointsCount * Globals.PointMoveManacost;
        if (forceMove || (enoughMana &&
            movedTiles.Count(t => t != null && t.TileData.Owner == GameEntity.Player) == movedTiles.Count))
        {
            // Only update moved
            foreach (TileScript tile in movedTiles)
                UpdateTerrainContinuity(tile);

            return movedPointsCount;
        }
        else
        {
            var changedAltitudes = ComputeAltitudeMap();

            // Restore original altitudes
            for (int y = 0; y <= sizeY; y++)
            {
                for (int x = 0; x <= sizeX; x++)
                {
                    int d = originalAltitudes[x, y] - changedAltitudes[x, y];
                    MovePointWithIndex(x, y, d);
                }
            }

            return enoughMana ? -1 : -2;
        }
    }

    public int[,] ComputeAltitudeMap()
    {
        // Compute array for easier altitude handling
        int[,] altitudes = new int[sizeX + 1, sizeY + 1];
        for (int y = 0; y < sizeY; y++)
        {
            for (int x = 0; x < sizeX; x++)
            {
                altitudes[x, y] = GetTile(x, y).TileData.AltitudeSW;
            }
        }
        for (int i = 0; i < sizeY; i++)
            altitudes[sizeX, i] = GetTile(sizeX - 1, i).TileData.AltitudeSE;

        for (int i = 0; i < sizeX; i++)
            altitudes[i, sizeY] = GetTile(i, sizeY - 1).TileData.AltitudeNW;

        altitudes[sizeX, sizeY] = GetTile(sizeX - 1, sizeY - 1).TileData.AltitudeNE;

        return altitudes;
    }
    public void ClearMap()
    {
        var tempList = gameField.Cast<Transform>().ToList();
        foreach (var child in tempList)
            Object.DestroyImmediate(child.gameObject);

        Tiles.Clear();
    }

    public static int GetMapIndex(float x, float y)
    {
        return GetMapIndex((int)x, (int)y);
    }

    public static int GetMapIndex(int x, int y)
    {
        return y * sizeX + x;
    }

    public static int GetMapIndex(TileScript tile)
    {
        return GetMapIndex(tile.GridPositionX, tile.GridPositionY);
    }

    public TileScript GetTile(int x, int y)
    {
        if (x < 0 || y < 0 || x >= sizeX || y >= sizeY)
            return null;

        return Tiles[GetMapIndex(x, y)];
    }

    public static float TileDistance(Vector2 a, Vector2 b)
    {
        return TileDistance(a.x, a.y, b.x, b.y);
    }

    public static float TileDistance(float x1, float y1, float x2, float y2)
    {
        return Mathf.Abs(Mathf.Sqrt(Mathf.Pow(x2 - x1, 2) + Mathf.Pow(y2 - y1, 2)));
    }

    public void UpdateMapFromBuffer(List<Tile> dataBuffer)
    {
        if (dataBuffer == null)
            return;
        for (int i = 0; i < Tiles.Count; i++)
            Tiles[i].CopyValuesFromBuffer(dataBuffer[i]);

        for (int i = 0; i < oldBuffer.Count; i++)
        {
            if (oldBuffer[i].TileType != dataBuffer[i].TileType && Tiles[i].TileData.TileType == oldBuffer[i].TileType ||
                ((Tiles[i].TileData.TileType == TileType.River && oldBuffer[i].TileType == TileType.Lake) || (Tiles[i].TileData.TileType == TileType.Lake && oldBuffer[i].TileType == TileType.River)))
            {
                ChangeTileType(Tiles[i], dataBuffer[i].TileType);
                Tiles[i].TileData.Owner = dataBuffer[i].Owner;
            }
        }

        UpdateMapView();
        oldBuffer = dataBuffer;
    }

    public List<Tile> GetTileListCopy()
    {
        return Tiles.Select(x => x.GetDataCopy()).ToList();
    }

    private void AddTile(int x, int y, int altitudeNE, int altitudeNW, int altitudeSE, int altitudeSW, TileType type, GameEntity owner)
    {
        GameObject tilePrefab =
            (GameObject)(Object.Instantiate(Resources.Load<GameObject>("Prefabs/DefaultTile"),
                new Vector3(x * 10f, 0, y * 10f), Quaternion.identity));
        tilePrefab.transform.SetParent(gameField);
        TileScript tile = tilePrefab.GetComponent<TileScript>();
        tile.transform.parent = gameField;
        tile.TileData.GridPosition = new Vector2(x, y);
        tile.TileData.AltitudeNE = altitudeNE;
        tile.TileData.AltitudeNW = altitudeNW;
        tile.TileData.AltitudeSE = altitudeSE;
        tile.TileData.AltitudeSW = altitudeSW;
        tile.UpdateAltitude();
        tile.TileData.TileType = type;
        tile.TileData.Owner = owner;
        Tiles.Add(tile);
        switch (type)
        {
            case TileType.None: break;
            case TileType.Forest: tile.TileData.Humidity = 0.25f; break;
            case TileType.Plain: tile.TileData.Humidity = 0.25f; break;
            case TileType.Swamp: tile.TileData.Humidity = 1f; break;
            case TileType.River: tile.TileData.Humidity = 1f; break;
            case TileType.Lake: tile.TileData.Humidity = 1f; break;
            case TileType.City: tile.TileData.Humidity = 0.25f; break;
            case TileType.Fire: tile.TileData.Humidity = 0.25f; break;
            case TileType.Desert: tile.TileData.Humidity = 0.25f; break;
            case TileType.Blizzard: tile.TileData.Humidity = 0.25f; break;
        }

        tile.SetupTerrainObjs();
        tile.UpdateTerrainObjsPlacement();
    }

    private void ReplaceTileWithNew(TileScript tile, string prefabName, float rotation)
    {
        ReplaceTileWithNew(tile, prefabName, rotation, tile.TileData.TileType);
    }

    private void ReplaceTileWithNew(TileScript tile, string prefabName, float rotation, TileType newTileType)
    {
        GameObject newTilePrefab =
            (GameObject)(Object.Instantiate(Resources.Load<GameObject>("Prefabs/" + prefabName),
                tile.transform.position, Quaternion.Euler(new Vector3(0f, rotation))));
        TileScript newTileScript = newTilePrefab.GetComponent<TileScript>();

        CopyAndSwapTileData(newTileScript, GetMapIndex(tile.GridPositionX, tile.GridPositionY));
        newTileScript.TileData.TileType = newTileType;
        
        newTileScript.UpdateTerrainObjsPlacement();
        newTileScript.UpdateAltitude();
        newTileScript.ChangeView(mapView);

        Object.Destroy(tile.gameObject);
    }

    private bool IsRiverOrLake(TileScript tile)
    {
        return tile != null && (tile.TileData.TileType == TileType.River || tile.TileData.TileType == TileType.Lake);
    }

    internal bool PlayerTileNearby(TileScript tile)
    {
        TileScript north = GetTile(tile.GridPositionX, tile.GridPositionY + 1);
        TileScript south = GetTile(tile.GridPositionX, tile.GridPositionY - 1);
        TileScript east = GetTile(tile.GridPositionX + 1, tile.GridPositionY);
        TileScript west = GetTile(tile.GridPositionX - 1, tile.GridPositionY);

        if (north != null && north.TileData.Owner == GameEntity.Player)
            return true;
        if (south != null && south.TileData.Owner == GameEntity.Player)
            return true;
        if (east != null && east.TileData.Owner == GameEntity.Player)
            return true;
        if (west != null && west.TileData.Owner == GameEntity.Player)
            return true;
        return false;
    }

    private void UpdateWaterContinuity(TileScript tile, bool recursive = true)
    {
        TileScript northWest = GetTile(tile.GridPositionX - 1, tile.GridPositionY + 1);
        TileScript north = GetTile(tile.GridPositionX, tile.GridPositionY + 1);
        TileScript northEast = GetTile(tile.GridPositionX + 1, tile.GridPositionY + 1);
        TileScript southWest = GetTile(tile.GridPositionX - 1, tile.GridPositionY - 1);
        TileScript south = GetTile(tile.GridPositionX, tile.GridPositionY - 1);
        TileScript southEast = GetTile(tile.GridPositionX + 1, tile.GridPositionY - 1);
        TileScript east = GetTile(tile.GridPositionX + 1, tile.GridPositionY);
        TileScript west = GetTile(tile.GridPositionX - 1, tile.GridPositionY);


        Action<string, float> setupNewRiver = (prefabName, rotation) => ReplaceTileWithNew(tile, prefabName, rotation, TileType.River);
        Action<string, float> setupNewLake = (prefabName, rotation) => ReplaceTileWithNew(tile, prefabName, rotation, TileType.Lake);
        // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Here be dragons ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //                                                        ____________
        //                                   (`-..________....---''  ____..._.-`
        //                                    \\`._______.._,.---'''     ,'
        //                                    ; )`.      __..-'`-.      /
        //                                   / /     _.-' _,.;;._ `-._,'
        //                                  / /   ,-' _.-'  //   ``--._``._
        //                                ,','_.-' ,-' _.- (( =-    -. `-._`-._____
        //                              ,;.''__..-'   _..--.\\.--'````--.._``-.`-._`.
        //               _          |\,' .-''        ```-'`---'`-...__,._  ``-.`-.`-.`.
        //    _     _.-,'(__)\__)\-'' `     ___  .          `     \      `--._
        //  ,',)---' /|)          `     `      ``-.   `     /     /     `     `-.
        //  \_____--.  '`  `               __..-.  \     . (   < _...-----..._   `.
        //   \_,--..__. \\ .-`.\----'';``,..-.__ \  \      ,`_. `.,-'`--'`---''`.  )
        //             `.\`.\  `_.-..' ,'   _,-..'  /..,-''(, ,' ; ( _______`___..'__
        //                     ((,(,__(    ((,(,__,'  ``'-- `'`.(\  `.,..______   SSt
        //                                                        ``--------..._``--.__
        if (IsRiverOrLake(tile))
        {
            // Nothing around -> small pond (technically river)
            if (!IsRiverOrLake(north) && !IsRiverOrLake(south) && !IsRiverOrLake(east) && !IsRiverOrLake(west))
            {
                setupNewRiver("RiverTile_O-Shape", 0f);
            }
            //
            //
            // River
            //
            //
            // Beginning or end
            //
            // West beginning or end
            else if (!IsRiverOrLake(north) && !IsRiverOrLake(south) && IsRiverOrLake(east) && !IsRiverOrLake(west))
            {
                setupNewRiver("RiverTile_Source", 0f);
            }
            // North beginning or end
            else if (!IsRiverOrLake(north) && IsRiverOrLake(south) && !IsRiverOrLake(east) && !IsRiverOrLake(west))
            {
                setupNewRiver("RiverTile_Source", 90f);
            }
            // East beginning or end
            else if (!IsRiverOrLake(north) && !IsRiverOrLake(south) && !IsRiverOrLake(east) && IsRiverOrLake(west))
            {
                setupNewRiver("RiverTile_Source", 180f);
            }
            // South beginning or end
            else if (IsRiverOrLake(north) && !IsRiverOrLake(south) && !IsRiverOrLake(east) && !IsRiverOrLake(west))
            {
                setupNewRiver("RiverTile_Source", 270f);
            }
            //
            // Straight
            //
            // Vertical
            else if (IsRiverOrLake(north) && IsRiverOrLake(south) && !IsRiverOrLake(east) && !IsRiverOrLake(west))
            {
                setupNewRiver("RiverTile_Straight", 0f);
            }
            // Horizontal
            else if (!IsRiverOrLake(north) && !IsRiverOrLake(south) && IsRiverOrLake(east) && IsRiverOrLake(west))
            {
                setupNewRiver("RiverTile_Straight", 90f);
            }
            //
            // River cross 3-way
            //
            // T
            else if (!IsRiverOrLake(north) && IsRiverOrLake(south) && IsRiverOrLake(east) && IsRiverOrLake(west) && !IsRiverOrLake(southWest) && !IsRiverOrLake(southEast))
            {
                setupNewRiver("RiverTile_T-Shape", 0f);
            }
            // T lying on right side
            else if (IsRiverOrLake(north) && IsRiverOrLake(south) && !IsRiverOrLake(east) && IsRiverOrLake(west) && !IsRiverOrLake(southWest) && !IsRiverOrLake(northWest))
            {
                setupNewRiver("RiverTile_T-Shape", 90f);
            }
            // T upside down
            else if (IsRiverOrLake(north) && !IsRiverOrLake(south) && IsRiverOrLake(east) && IsRiverOrLake(west) && !IsRiverOrLake(northWest) && !IsRiverOrLake(northEast))
            {
                setupNewRiver("RiverTile_T-Shape", 180f);
            }
            // T lying on left side
            else if (IsRiverOrLake(north) && IsRiverOrLake(south) && IsRiverOrLake(east) && !IsRiverOrLake(west) && !IsRiverOrLake(southEast) && !IsRiverOrLake(northEast))
            {
                setupNewRiver("RiverTile_T-Shape", 270f);
            }
            //
            // River cross 4-way
            //
            // X
            else if (IsRiverOrLake(north) && IsRiverOrLake(south) && IsRiverOrLake(east) && IsRiverOrLake(west) &&
                     !IsRiverOrLake(northWest) && !IsRiverOrLake(northEast) && !IsRiverOrLake(southWest) && !IsRiverOrLake(southEast))
            {
                setupNewRiver("RiverTile_X-Shape", 0f);
            }
            //
            // Corner
            //
            // NE Corner of river
            else if (IsRiverOrLake(west) && !IsRiverOrLake(southWest) && IsRiverOrLake(south) && !IsRiverOrLake(north) && !IsRiverOrLake(east))
            {
                setupNewRiver("RiverTile_Corner", 90f);
            }
            // SE Corner of river
            else if (!IsRiverOrLake(northWest) && IsRiverOrLake(north) && IsRiverOrLake(west) && !IsRiverOrLake(south) && !IsRiverOrLake(east))
            {
                setupNewRiver("RiverTile_Corner", 180f);
            }
            // SW Corner of river
            else if (IsRiverOrLake(north) && !IsRiverOrLake(northEast) && IsRiverOrLake(east) && !IsRiverOrLake(south) && !IsRiverOrLake(west))
            {
                setupNewRiver("RiverTile_Corner", 270f);
            }
            // NW Corner of river
            else if (IsRiverOrLake(east) && !IsRiverOrLake(southEast) && IsRiverOrLake(south) && !IsRiverOrLake(north) && !IsRiverOrLake(west))
            {
                setupNewRiver("RiverTile_Corner", 0f);
            }
            //
            //
            //
            // Lake
            //
            //
            //
            // Whole tile is lake
            else if (IsRiverOrLake(north) && IsRiverOrLake(south) && IsRiverOrLake(east) && IsRiverOrLake(west) &&
                     IsRiverOrLake(northWest) && IsRiverOrLake(northEast) && IsRiverOrLake(southWest) &&
                     IsRiverOrLake(southEast))
            {
                setupNewLake("LakeTile_Whole", 0f);
            }
            //
            // One corner is grass
            //
            // NE Corner is grass
            else if (IsRiverOrLake(north) && IsRiverOrLake(south) && IsRiverOrLake(east) && IsRiverOrLake(west) &&
                     IsRiverOrLake(northWest) && !IsRiverOrLake(northEast) && IsRiverOrLake(southWest) &&
                     IsRiverOrLake(southEast))
            {
                setupNewLake("LakeTile_Tip", 0f);

            }
            // SE Corner is grass
            else if (IsRiverOrLake(north) && IsRiverOrLake(south) && IsRiverOrLake(east) && IsRiverOrLake(west) &&
                     IsRiverOrLake(northWest) && IsRiverOrLake(northEast) && IsRiverOrLake(southWest) &&
                     !IsRiverOrLake(southEast))
            {
                setupNewLake("LakeTile_Tip", 90f);

            }
            // SW Corner is grass
            else if (IsRiverOrLake(north) && IsRiverOrLake(south) && IsRiverOrLake(east) && IsRiverOrLake(west) &&
                     IsRiverOrLake(northWest) && IsRiverOrLake(northEast) && !IsRiverOrLake(southWest) &&
                     IsRiverOrLake(southEast))
            {
                setupNewLake("LakeTile_Tip", 180f);

            }
            // NW Corner is grass
            else if (IsRiverOrLake(north) && IsRiverOrLake(south) && IsRiverOrLake(east) && IsRiverOrLake(west) &&
                     !IsRiverOrLake(northWest) && IsRiverOrLake(northEast) && IsRiverOrLake(southWest) &&
                     IsRiverOrLake(southEast))
            {
                setupNewLake("LakeTile_Tip", 270f);

            }
            //
            // Diagonal corners are grass
            //
            // NE and SW Corner is grass
            else if (IsRiverOrLake(north) && IsRiverOrLake(south) && IsRiverOrLake(east) && IsRiverOrLake(west) &&
                     IsRiverOrLake(northWest) && !IsRiverOrLake(northEast) && !IsRiverOrLake(southWest) &&
                     IsRiverOrLake(southEast))
            {
                setupNewLake("LakeTile_DiagonalTips", 0f);

            }
            // SE and NW Corner is grass
            else if (IsRiverOrLake(north) && IsRiverOrLake(south) && IsRiverOrLake(east) && IsRiverOrLake(west) &&
                     !IsRiverOrLake(northWest) && IsRiverOrLake(northEast) && IsRiverOrLake(southWest) &&
                     !IsRiverOrLake(southEast))
            {
                setupNewLake("LakeTile_DiagonalTips", 90f);

            }
            //
            // Simple shore
            //
            // N Side of lake
            else if (IsRiverOrLake(west) && IsRiverOrLake(southWest) && IsRiverOrLake(south) && IsRiverOrLake(southEast) && IsRiverOrLake(east) && !IsRiverOrLake(north))
            {
                setupNewLake("LakeTile_Side", 180f);
            }
            // S Side of lake
            else if (IsRiverOrLake(west) && IsRiverOrLake(northWest) && IsRiverOrLake(north) && IsRiverOrLake(northEast) && IsRiverOrLake(east) && !IsRiverOrLake(south))
            {
                setupNewLake("LakeTile_Side", 0f);
            }
            // E Side of lake
            else if (IsRiverOrLake(north) && IsRiverOrLake(northEast) && IsRiverOrLake(east) && IsRiverOrLake(southEast) && IsRiverOrLake(south) && !IsRiverOrLake(west))
            {
                setupNewLake("LakeTile_Side", 90f);
            }
            // W Side of lake
            else if (IsRiverOrLake(north) && IsRiverOrLake(northWest) && IsRiverOrLake(west) && IsRiverOrLake(southWest) && IsRiverOrLake(south) && !IsRiverOrLake(east))
            {
                setupNewLake("LakeTile_Side", 270f);
            }
            //
            // Lake side with delta
            //
            // N Side of lake
            else if (IsRiverOrLake(west) && IsRiverOrLake(southWest) && IsRiverOrLake(south) && IsRiverOrLake(southEast) && IsRiverOrLake(east) && IsRiverOrLake(north))
            {
                setupNewLake("LakeTile_SideRiverDelta", 180f);
            }
            // S Side of lake
            else if (IsRiverOrLake(west) && IsRiverOrLake(northWest) && IsRiverOrLake(north) && IsRiverOrLake(northEast) && IsRiverOrLake(east) && IsRiverOrLake(south))
            {
                setupNewLake("LakeTile_SideRiverDelta", 0f);
            }
            // E Side of lake
            else if (IsRiverOrLake(north) && !IsRiverOrLake(northEast) && IsRiverOrLake(east) && !IsRiverOrLake(southEast) && IsRiverOrLake(south) && IsRiverOrLake(west) &&
                    IsRiverOrLake(southWest) && IsRiverOrLake(northWest))
            {
                setupNewLake("LakeTile_SideRiverDelta", 270f);
            }
            // W Side of lake
            else if (IsRiverOrLake(north) && !IsRiverOrLake(northWest) && IsRiverOrLake(west) && !IsRiverOrLake(southWest) && IsRiverOrLake(south) && IsRiverOrLake(east) &&
                IsRiverOrLake(southEast) && IsRiverOrLake(northEast))
            {
                setupNewLake("LakeTile_SideRiverDelta", 90f);
            }
            //
            // Lake corner with delta
            //
            // Double delta
            //
            else if (IsRiverOrLake(west) && IsRiverOrLake(southWest) && IsRiverOrLake(south) && IsRiverOrLake(east) && IsRiverOrLake(north))
            {
                setupNewLake("LakeTile_CornerBothRiverDelta", 180f);
            }
            // SE Corner of lake with delta east
            else if (IsRiverOrLake(northWest) && IsRiverOrLake(north) && IsRiverOrLake(west) && IsRiverOrLake(east) && IsRiverOrLake(south))
            {
                setupNewLake("LakeTile_CornerBothRiverDelta", 270f);
            }
            // SW Corner of lake with delta west
            else if (IsRiverOrLake(north) && IsRiverOrLake(northEast) && IsRiverOrLake(east) && IsRiverOrLake(west) && IsRiverOrLake(south))
            {
                setupNewLake("LakeTile_CornerBothRiverDelta", 0f);
            }
            // NW Corner of lake with delta west
            else if (IsRiverOrLake(east) && IsRiverOrLake(southEast) && IsRiverOrLake(south) && IsRiverOrLake(west) && IsRiverOrLake(north))
            {
                setupNewLake("LakeTile_CornerBothRiverDelta", 90f);
            }
            //
            // Single corner delta
            //
            // NE Corner of lake with delta east
            else if (IsRiverOrLake(west) && IsRiverOrLake(southWest) && IsRiverOrLake(south) && IsRiverOrLake(east))
            {
                setupNewLake("LakeTile_CornerRightRiverDelta", 180f);
            }
            // NE Corner of lake with delta north
            else if (IsRiverOrLake(west) && IsRiverOrLake(southWest) && IsRiverOrLake(south) && IsRiverOrLake(north))
            {
                setupNewLake("LakeTile_CornerLeftRiverDelta", 180f);
            }
            // SE Corner of lake with delta east
            else if (IsRiverOrLake(northWest) && IsRiverOrLake(north) && IsRiverOrLake(west) && IsRiverOrLake(east))
            {
                setupNewLake("LakeTile_CornerLeftRiverDelta", 270f);
            }
            // SE Corner of lake with delta south
            else if (IsRiverOrLake(northWest) && IsRiverOrLake(north) && IsRiverOrLake(west) && IsRiverOrLake(south))
            {
                setupNewLake("LakeTile_CornerRightRiverDelta", 270f);
            }
            // SW Corner of lake with delta west
            else if (IsRiverOrLake(north) && IsRiverOrLake(northEast) && IsRiverOrLake(east) && IsRiverOrLake(west))
            {
                setupNewLake("LakeTile_CornerRightRiverDelta", 0f);
            }
            // SW Corner of lake with delta south
            else if (IsRiverOrLake(north) && IsRiverOrLake(northEast) && IsRiverOrLake(east) && IsRiverOrLake(south))
            {
                setupNewLake("LakeTile_CornerLeftRiverDelta", 0f);
            }
            // NW Corner of lake with delta west
            else if (IsRiverOrLake(east) && IsRiverOrLake(southEast) && IsRiverOrLake(south) && IsRiverOrLake(west))
            {
                setupNewLake("LakeTile_CornerLeftRiverDelta", 90f);
            }
            // NW Corner of lake with delta north
            else if (IsRiverOrLake(east) && IsRiverOrLake(southEast) && IsRiverOrLake(south) && IsRiverOrLake(north))
            {
                setupNewLake("LakeTile_CornerRightRiverDelta", 90f);
            }
            //
            // Lake corner
            //
            // NE Corner of lake
            else if (IsRiverOrLake(west) && IsRiverOrLake(southWest) && IsRiverOrLake(south))
            {
                setupNewLake("LakeTile_Corner", 0f);
            }
            // SE Corner of lake
            else if (IsRiverOrLake(northWest) && IsRiverOrLake(north) && IsRiverOrLake(west))
            {
                setupNewLake("LakeTile_Corner", 90f);
            }
            // SW Corner of lake
            else if (IsRiverOrLake(north) && IsRiverOrLake(northEast) && IsRiverOrLake(east))
            {
                setupNewLake("LakeTile_Corner", 180f);
            }
            // NW Corner of lake
            else if (IsRiverOrLake(east) && IsRiverOrLake(southEast) && IsRiverOrLake(south))
            {
                setupNewLake("LakeTile_Corner", 270f);
            }
        }
        // Not water
        else
        {
            UpdateTerrainContinuity(tile);
        }

        if (recursive)
        {
            if (IsRiverOrLake(northWest)) UpdateWaterContinuity(northWest, false);
            if (IsRiverOrLake(north)) UpdateWaterContinuity(north, false);
            if (IsRiverOrLake(northEast)) UpdateWaterContinuity(northEast, false);
            if (IsRiverOrLake(east)) UpdateWaterContinuity(east, false);
            if (IsRiverOrLake(southEast)) UpdateWaterContinuity(southEast, false);
            if (IsRiverOrLake(south)) UpdateWaterContinuity(south, false);
            if (IsRiverOrLake(southWest)) UpdateWaterContinuity(southWest, false);
            if (IsRiverOrLake(west)) UpdateWaterContinuity(west, false);
        }
    }

    private void UpdateTerrainContinuity(TileScript tile)
    {
        if (tile == null || IsRiverOrLake(tile))
            return;

        int altNW = tile.TileData.AltitudeNW;
        int altNE = tile.TileData.AltitudeNE;
        int altSW = tile.TileData.AltitudeSW;
        int altSE = tile.TileData.AltitudeSE;

        Func<int, int, int, int, bool> allEquals = (first, second, third, fourth) => first == second && third == fourth && first == third;

        // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ More dragons ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //                                            _   __,----'~~~~~~~~~`-----.__
        //                                     .  .    `//====-              ____,-'~`
        //                     -.            \_|// .   /||\\  `~~~~`---.___./
        //               ______-==.       _-~o  `\/    |||  \\           _,'`
        //         __,--'   ,=='||\=_    ;_,_,/ _-'|-   |`\   \\        ,'
        //      _-'      ,='    | \\`.    '',/~7  /-   /  ||   `\.     /
        //    .'       ,'       |  \\  \_  "  /  /-   /   ||      \   /
        //   / _____  /         |     \\.`-_/  /|- _/   ,||       \ /
        //  ,-'     `-|--'~~`--_ \     `==-/  `| \'--===-'       _/`
        //            '         `-|      /|    )-'\~'      _,--"'
        //                        '-~^\_/ |    |   `\_   ,^             /\
        //                             /  \     \__   \/~               `\__
        //                         _,-' _/'\ ,-'~____-'`-/                 ``===\
        //                        ((->/'    \|||' `.     `\.  ,                _||
        //                                   \_     `\      `~---|__i__i__\--~'_/
        //                                  __-^-_    `)  \-.______________,-~'
        //                                 ///,-'~`__--^-  |-------~~~~^'
        //                                        ///,--~`-\
        // Flat
        if (allEquals(altNW, altNE, altSE, altSW))
        {
            ReplaceTileWithNew(tile, "DefaultTile", 0f);
        }
        // 
        //
        // Change by one
        //
        //
        //
        // Two by one
        //
        // South is higher
        else if (allEquals(altNW + 1, altNE + 1, altSW, altSE))
        {
            ReplaceTileWithNew(tile, "DefaultTile_TwoPointsByTwo", 0f);
        }
        // West is higher
        else if (allEquals(altNW, altNE + 1, altSW, altSE + 1))
        {
            ReplaceTileWithNew(tile, "DefaultTile_TwoPointsByTwo", 90f);
        }
        // North is higher
        else if (allEquals(altNW, altNE, altSW + 1, altSE + 1))
        {
            ReplaceTileWithNew(tile, "DefaultTile_TwoPointsByTwo", 180f);
        }
        // East is higher
        else if (allEquals(altNW + 1, altNE, altSW + 1, altSE))
        {
            ReplaceTileWithNew(tile, "DefaultTile_TwoPointsByTwo", 270f);
        }
        //
        // One corner up by one
        //
        // NW is higher
        else if (allEquals(altNW - 1, altNE, altSW, altSE))
        {
            ReplaceTileWithNew(tile, "DefaultTile_OnePointByTwo", 90f);
        }
        // NE is higher
        else if (allEquals(altNW, altNE - 1, altSW, altSE))
        {
            ReplaceTileWithNew(tile, "DefaultTile_OnePointByTwo", 180f);
        }
        // SE is higher
        else if (allEquals(altNW, altNE, altSW, altSE - 1))
        {
            ReplaceTileWithNew(tile, "DefaultTile_OnePointByTwo", 270f);
        }
        // SW is higher
        else if (allEquals(altNW, altNE, altSW - 1, altSE))
        {
            ReplaceTileWithNew(tile, "DefaultTile_OnePointByTwo", 0f);
        }
        //
        // One corner down by one
        //
        // NW is lower
        else if (allEquals(altNW + 1, altNE, altSW, altSE))
        {
            ReplaceTileWithNew(tile, "DefaultTile_ThreePointsByTwo", 0f);
        }
        // NE is lower
        else if (allEquals(altNW, altNE + 1, altSW, altSE))
        {
            ReplaceTileWithNew(tile, "DefaultTile_ThreePointsByTwo", 90f);
        }
        // SE is lower
        else if (allEquals(altNW, altNE, altSW, altSE + 1))
        {
            ReplaceTileWithNew(tile, "DefaultTile_ThreePointsByTwo", 180f);
        }
        // SW is lower
        else if (allEquals(altNW, altNE, altSW + 1, altSE))
        {
            ReplaceTileWithNew(tile, "DefaultTile_ThreePointsByTwo", 270f);
        }
        //
        // Diagonal by one
        //
        // NW is higher
        else if (allEquals(altNW - 1, altNE, altSW, altSE + 1))
        {
            ReplaceTileWithNew(tile, "DefaultTile_DiagonalByTwo", 90f);
        }
        // NE is higher
        else if (allEquals(altNW, altNE - 1, altSW + 1, altSE))
        {
            ReplaceTileWithNew(tile, "DefaultTile_DiagonalByTwo", 180f);
        }
        // SE is higher
        else if (allEquals(altNW + 1, altNE, altSW, altSE - 1))
        {
            ReplaceTileWithNew(tile, "DefaultTile_DiagonalByTwo", 270f);
        }
        // SW is higher
        else if (allEquals(altNW, altNE + 1, altSW - 1, altSE))
        {
            ReplaceTileWithNew(tile, "DefaultTile_DiagonalByTwo", 0f);
        }
        //
        // Diagonals are the same
        //
        // NW = SE
        else if (allEquals(altNW + 1, altNE, altSW, altSE + 1))
        {
            ReplaceTileWithNew(tile, "DefaultTile_DiagonalTwoPointByTwoSameAltitudeUp", 0f);
        }
        else if (allEquals(altNW, altNE + 1, altSW + 1, altSE))
        {
            ReplaceTileWithNew(tile, "DefaultTile_DiagonalTwoPointByTwoSameAltitudeUp", 90f);
        }
        //
        //
        // Change by two
        // Fuck that
        //
        //
    }
}

