﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Classes
{
    public static class ExtensionMethods
    {
        public static T RandomElement<T>(this IEnumerable<T> collection)
        {
            return collection.ElementAt(Random.Range(0, collection.Count()));
        }
    }
}
