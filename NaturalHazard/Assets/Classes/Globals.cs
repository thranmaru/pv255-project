﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

internal static class Globals
{
    private const TileType Disasters = TileType.Blizzard | TileType.Desert | TileType.Fire | TileType.Swamp;
    private const TileType Nature = TileType.Forest | TileType.Lake | TileType.Plain | TileType.River;
    private const TileType Devastated = TileType.FrozenLand | TileType.WitheredLand;

    public static string[] MapNames = new string[] { "1", "2", "3", "4", "5" };
    public const string SandboxMapName = "sandbox";
    public static Dictionary<TileType, string> WeatherForecasts = new Dictionary<TileType,string>();

    internal const int TileBuyManacost = 50;
    internal const int PointMoveManacost = 20;

    static Globals()
    {
        WeatherForecasts.Add(TileType.None, "The weather is calm today, you can just practice your powers and build nice landscape.");
        WeatherForecasts.Add(TileType.Fire, "Wild fires are expected to break out in {0} minutes.");
        WeatherForecasts.Add(TileType.Blizzard, "Dangerous blizzard is coming in about {0} minutes.");
        WeatherForecasts.Add(TileType.Desert, "The rude sand storm is coming in about {0} minutes.");
        WeatherForecasts.Add(TileType.Swamp, "There was a breach in a nearby dam. Our surroundings are going to turn into swamps in about {0} minutes.");
    }

    internal static int GetBiomChangeManacost(TileType from, TileType to)
    {
        switch (from)
        {
            default:
                switch (to)
                {
                    case TileType.Forest:
                        return 100;
                    case TileType.Plain:
                        return 20;
                    case TileType.River:
                        return 200;
                    default:
                        return int.MaxValue;
                }
        }
    }
    internal const float FireExpectedLife = 30f;
    internal const float FireNewSpawnDelay = 10f;
    internal const float FireTempSensitivity = 10f;
    internal const float FireHumiditySensitivity = 10f;
    internal const float FireSpawnSensitivityRatio = 1f;

    internal const float DesertExpectedLife = 180f;
    internal const float DesertNewSpawnDelay = 20;
    internal const float DesertTempSensitivity = 50f;
    internal const float DesertHumiditySensitivity = 50f;
    internal const float DesertSpawnSensitivityRatio = 10f;

    internal const float BlizzardExpectedLife = 30f;
    internal const float BlizzardNewSpawnDelay = 10f;
    internal const float BlizzardTempSensitivity = 10f;
    internal const float BlizzardHumiditySensitivity = 10f;
    internal const float BlizzardSpawnSensitivityRatio = 1.5f;

    internal const float SwampExpectedLife = 180f;
    internal const float SwampNewSpawnDelay = 30f;
    internal const float SwampTempSensitivity = 25f;
    internal const float SwampHumiditySensitivity = 50f;
    internal const float SwampSpawnSensitivityRatio = 15f;

    internal static bool IsDisaster(this Tile tile)
    {
        return IsDisaster(tile.TileType);
    }

    internal static bool IsNature(this Tile tile)
    {
        return IsNature(tile.TileType);
    }

    internal static bool IsDevastated(this Tile tile)
    {
        return IsDevastated(tile.TileType);
    }

    internal static bool IsDisaster(this TileType tileType)
    {
        return (tileType & Disasters) == tileType;
    }

    internal static bool IsNature(this TileType tileType)
    {
        return (tileType & Nature) == tileType;
    }

    internal static bool IsDevastated(this TileType tileType)
    {
        return (tileType & Devastated) == tileType;
    }
    internal static float TempClamp(float temp)
    {
        return UnityEngine.Mathf.Clamp(temp, -50f, 50f);
    }

    internal static int TileTypeToDisasterIndex(TileType tileType)
    {
        switch (tileType)
        {
            case TileType.Fire:
                return 0;
            case TileType.Blizzard:
                return 1;
            case TileType.Desert:
                return 2;
            case TileType.Swamp:
                return 3;
            default:
                return 0;
        }
    }
}
