﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[InitializeOnLoad]
public class OnUnityLoad
{

    static OnUnityLoad()
    {

        EditorApplication.playmodeStateChanged = () =>
        {
            if (!EditorApplication.isCompiling && !EditorApplication.isPlaying)
            {
                Debug.Log("Auto-Saving scene: " + EditorApplication.currentScene);

                EditorApplication.SaveScene();
                AssetDatabase.SaveAssets();
            }
        };

    }

}