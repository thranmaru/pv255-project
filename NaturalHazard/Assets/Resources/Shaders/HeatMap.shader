﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/HeatMap"
{
    Properties
    {
        Temperature ("Temperature", Float) = 0
    }

    SubShader
    {
        Tags { "RenderType" = "Geometry" }
        
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            float Temperature;

            struct vertexInput
            {
                float4 vertex : POSITION;
            };

            struct fragmentInput
            {
                float4 position : SV_POSITION;
            };

            fragmentInput vert(vertexInput i)
            {
                fragmentInput o;
                o.position = UnityObjectToClipPos(i.vertex);
                return o;
            }

            float4 frag(fragmentInput i) : COLOR
            {
                float scale = (Temperature + 40) / 100;

                float red = clamp((4 * scale) - 2, 0.0, 1.0);
                float green = 0.8;
                if (scale < 0.25)
                    green = 3.2 * scale;
                else if (scale > 0.75)
                    green = 4 - (4 * scale);
                float blue = clamp(2 - (4 * scale), 0.0, 1.0);

                return float4(red, green, blue, 1.0);
            }
            ENDCG
        }
    }
}

