﻿Shader "Custom/Highlight"
{
	Properties
	{
		_ColorTint("Color Tint", Color) = (1, 1, 1, 1)
		_MainTex("Base (RGB)", 2D) = "white" {}
		_RimColor("Rim Color", Color) = (0.2, 0.2, 1, 1)
		_RimPower("Rim Power", Range(1.0, 6.0)) = 3.0
	}
	SubShader
	{
		Tags { "RenderType" = "Opaque" }

		CGPROGRAM
		#pragma surface surf Lambert

		struct Input
		{
			float4 color : Color;
			float2 uv_MainTex;
			float2 uv_BumpMap;
			float3 viewDir;
		};

		float4 _ColorTint;
		sampler2D _MainTex;
		sampler2D _BumpMap;
		float4 _RimColor;
		float _RimPower;

		void surf(Input input, inout SurfaceOutput output)
		{
			input.color = _ColorTint;
			output.Albedo = tex2D(_MainTex, input.uv_MainTex).rgb * input.color;

			half rim = 1.0 - saturate(dot(normalize(input.viewDir), float3(0.0, 1.0, 0.0)));
			output.Emission = _RimColor.rgb * pow(rim, _RimPower);
		}
		ENDCG
	}
	FallBack "Diffuse"
}