﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/HumidityMap"
{
    Properties
    {
        Humidity ("Humidity", Float) = 0
    }

    SubShader
    {
        Tags { "RenderType" = "Geometry" }
        
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            float Humidity;

            struct vertexInput
            {
                float4 vertex : POSITION;
            };

            struct fragmentInput
            {
                float4 position : SV_POSITION;
            };

            fragmentInput vert(vertexInput i)
            {
                fragmentInput o;
                o.position = UnityObjectToClipPos(i.vertex);
                return o;
            }

            float4 frag(fragmentInput i) : COLOR
            {
                return float4(1 - (Humidity / 0.8), 1 - (Humidity / 1.5), 1.0 - (Humidity / 5), 1.0);
            }
            ENDCG
        }
    }
}

