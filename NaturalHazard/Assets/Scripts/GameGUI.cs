﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

enum PlayerTileAction { BuyTile, Forest, Plain, River, Up, Down, None }

public class GameGUI : MonoBehaviour
{
    public Transform GameManagerTransform;
    public GUISkin Skin;
    public Canvas GameGUICanvas;
    public Canvas PointUpDownCanvas;

    public Transform ManaPanel;
    public GameObject ManaSpentTextPrefab;
    public Text ManaText;
    public Text ManaIncrementText;

    // Weather forecast
    public GameObject ForecastPanelPrefab;

    // Tooltip UI
    public GameObject TooltipPanel;
    public Text TileTypeText;
    public Text HumidityText;
    public Text TemperatureText;
    public Text ManaGenText;
    public Image TooltipImage;
    private Rect tooltipPanelRect;

    private GameObject forecastPanel;

    private GameManager gameManager;
    private TileScript highlightedTile;
    private TileScript mouseDownTileLastFrame;

    // For changing altitude
    private List<GameObject> pointSelectedImages = new List<GameObject>();
    private List<Point> selectedPointsIndices = new List<Point>();

    private Action<TileScript> tileMouseDownAction;
    private PlayerTileAction playerTileAction = PlayerTileAction.None;

    private Point moveUpDownStartPointIndex = new Point(-1, -1);

    private Dictionary<TileType, Sprite> tooltipSprites = new Dictionary<TileType, Sprite>();

    public Button forestBtn, riverBtn, plainsBtn, moveUpBtn, moveDownBtn, buyBtn, natureMapBtn, humidityMapBtn, tempMapBtn;

    private void Start()
    {
        gameManager = GameManagerTransform.GetComponent<GameManager>();

        tooltipSprites.Add(TileType.Forest, Resources.Load<Sprite>("GUI Elements/Icons/ForestIcon"));
        tooltipSprites.Add(TileType.Plain, Resources.Load<Sprite>("GUI Elements/Icons/PlainIcon"));
        tooltipSprites.Add(TileType.River, Resources.Load<Sprite>("GUI Elements/Icons/RiverIcon"));
        tooltipSprites.Add(TileType.Lake, Resources.Load<Sprite>("GUI Elements/Icons/LakeIcon"));
        tooltipSprites.Add(TileType.City, Resources.Load<Sprite>("GUI Elements/Icons/CityIcon"));
        tooltipSprites.Add(TileType.Fire, Resources.Load<Sprite>("GUI Elements/Icons/FireIcon"));
        tooltipSprites.Add(TileType.Desert, Resources.Load<Sprite>("GUI Elements/Icons/DesertIcon"));
        tooltipSprites.Add(TileType.Blizzard, Resources.Load<Sprite>("GUI Elements/Icons/BlizzardIcon"));
        tooltipSprites.Add(TileType.Swamp, Resources.Load<Sprite>("GUI Elements/Icons/SwampIcon"));
        tooltipSprites.Add(TileType.FrozenLand, Resources.Load<Sprite>("GUI Elements/Icons/FrozenLandIcon"));
        tooltipSprites.Add(TileType.WitheredLand, Resources.Load<Sprite>("GUI Elements/Icons/WitheredLandIcon"));

        var tmpRect = TooltipPanel.GetComponent<RectTransform>().rect;
        tooltipPanelRect = new Rect(Screen.width - tmpRect.width * GameGUICanvas.scaleFactor, 0, tmpRect.width * GameGUICanvas.scaleFactor, tmpRect.height * GameGUICanvas.scaleFactor);

        forecastPanel = (GameObject)UnityEngine.Object.Instantiate(ForecastPanelPrefab);
        forecastPanel.transform.SetParent(GameGUICanvas.transform);
        forecastPanel.transform.localScale = Vector3.one;
        forecastPanel.GetComponent<RectTransform>().sizeDelta = new Vector2(336f, 153f);
        forecastPanel.GetComponent<RectTransform>().localPosition = Vector3.zero;
        forecastPanel.GetComponent<RectTransform>().rotation = GameGUICanvas.transform.rotation;
        forecastPanel.GetComponent<RectTransform>().anchoredPosition = new Vector2(0f, 1f);
        Button b = (Button)forecastPanel.transform.GetChild(2).GetComponent<Button>();
        b.onClick.AddListener(() => Destroy(forecastPanel));
        Text t = (Text)forecastPanel.transform.GetChild(1).GetComponent<Text>();
        string text = "No forecast for today.";
        text = string.Format(Globals.WeatherForecasts[gameManager.Map.Scenario.DisasterType], (gameManager.Map.Scenario.SpawningTime / 60f).ToString("0.0"));
        t.text = text;
    }

    private void Update()
    {
        HandleKeyboard();
        HandleMouse();

        ManaText.text = ((int)gameManager.Mana).ToString();
        ManaIncrementText.text = "+" + ((int)(gameManager.ManaDelta * 10)).ToString();

        Vector2 mousePositionInvertedY = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);
        // Tile info
        if (highlightedTile && !tooltipPanelRect.Contains(mousePositionInvertedY))
        {
            TooltipImage.sprite = tooltipSprites[highlightedTile.TileData.TileType];
            TileTypeText.text = highlightedTile.TileData.TileType.ToString();
            HumidityText.text = ((int)(highlightedTile.TileData.Humidity * 100)).ToString();
            TemperatureText.text = ((int)(highlightedTile.TileData.Temperature)).ToString();
            ManaGenText.text = ((highlightedTile.TileData.ManaGeneration * 10)).ToString("0.00");
            TooltipPanel.SetActive(true);
        }
        else
        {
            TooltipPanel.SetActive(false);
        }
    }

    private void PlayManaSpenAnim(int manaCost)
    {
        GameObject g = (GameObject)UnityEngine.Object.Instantiate(ManaSpentTextPrefab, ManaPanel.position, ManaPanel.rotation);
        g.transform.SetParent(ManaPanel);
        g.transform.localScale = Vector3.one;
        g.GetComponent<RectTransform>().sizeDelta = new Vector2(50f, 50f);
        g.GetComponent<RectTransform>().position = ManaPanel.position;
        g.GetComponent<RectTransform>().anchoredPosition = new Vector2(0.5f, 0f);
        Text t = (Text)(g.GetComponent<Text>());
        t.text = "-" + manaCost.ToString();
        var anim = g.GetComponent<Animator>();
        anim.Play("Mana Spent");

        StartCoroutine(DestroyManaSpentAnim(g));
    }

    IEnumerator DestroyManaSpentAnim(GameObject manaSpentText)
    {
        yield return new WaitForSeconds(1);
        Destroy(manaSpentText);
    }

    public void SelectForest()
    {
        playerTileAction = PlayerTileAction.Forest;
        tileMouseDownAction = (t) =>
        {
            int manaCost = gameManager.ChangeTileType(t, TileType.Forest);
            if (manaCost > 0) PlayManaSpenAnim(manaCost);
        };
    }

    public void SelectPlain()
    {
        playerTileAction = PlayerTileAction.Plain;
        tileMouseDownAction = (t) =>
        {
            int manaCost = gameManager.ChangeTileType(t, TileType.Plain);
            if (manaCost > 0) PlayManaSpenAnim(manaCost);
        };
    }

    public void SelectWater()
    {
        playerTileAction = PlayerTileAction.River;
        tileMouseDownAction = (t) =>
        {
            int manaCost = gameManager.ChangeTileType(t, TileType.River);
            if (manaCost > 0) PlayManaSpenAnim(manaCost);
        };
    }

    public void SelectUp()
    {
        playerTileAction = PlayerTileAction.Up;
        tileMouseDownAction = null;
    }

    public void SelectDown()
    {
        playerTileAction = PlayerTileAction.Down;
        tileMouseDownAction = null;
    }

    public void SelectBuyTile()
    {
        playerTileAction = PlayerTileAction.BuyTile;
        tileMouseDownAction = (t) =>
        {
            int manaCost = gameManager.BuyTile(t);
            if (manaCost > 0) PlayManaSpenAnim(manaCost);
        };
    }

    public void SelectNormalView()
    {
        gameManager.ChangeMapView(MapView.Normal);
    }

    public void SelectHeatMapView()
    {
        gameManager.ChangeMapView(MapView.Temperature);
    }

    public void SelectHumidityView()
    {
        gameManager.ChangeMapView(MapView.Humidity);
    }

    private void OnGUI()
    {
#if UNITY_EDITOR
        GUI.skin = Skin;

        // Menu
        if (GUI.Button(new Rect(0f, 0f, 100f, 25f), "Back to Editor"))
        {
            gameManager.ChangeGameState(GameState.Editor);
            GetComponentInParent<EditorGUI>().enabled = true;
            enabled = false;
        }
#endif
    }

    private void HandleKeyboard()
    {
        /*
        if (Input.GetKeyDown(KeyCode.Escape))
            tileMouseDownAction = null;*/

        var pointer = new PointerEventData(EventSystem.current);
        if (Input.GetAxis("Cancel") > 0f)
            tileMouseDownAction = null;
        if (Input.GetAxis("Select plains tool") > 0f)
            ExecuteEvents.Execute(plainsBtn.gameObject, pointer, ExecuteEvents.submitHandler);
        else if (Input.GetAxis("Select forest tool") > 0f)
            ExecuteEvents.Execute(forestBtn.gameObject, pointer, ExecuteEvents.submitHandler);
        else if (Input.GetAxis("Select water tool") > 0f)
            ExecuteEvents.Execute(riverBtn.gameObject, pointer, ExecuteEvents.submitHandler);
        else if (Input.GetAxis("Select move up tool") > 0f)
            ExecuteEvents.Execute(moveUpBtn.gameObject, pointer, ExecuteEvents.submitHandler);
        else if (Input.GetAxis("Select move down tool") > 0f)
            ExecuteEvents.Execute(moveDownBtn.gameObject, pointer, ExecuteEvents.submitHandler);
        else if (Input.GetAxis("Buy tool") > 0f)
            ExecuteEvents.Execute(buyBtn.gameObject, pointer, ExecuteEvents.submitHandler);
        if (Input.GetAxis("View nature map") > 0f)
            ExecuteEvents.Execute(natureMapBtn.gameObject, pointer, ExecuteEvents.submitHandler);
        else if (Input.GetAxis("View humidity map") > 0f)
            ExecuteEvents.Execute(humidityMapBtn.gameObject, pointer, ExecuteEvents.submitHandler);
        else if (Input.GetAxis("View temperature map") > 0f)
            ExecuteEvents.Execute(tempMapBtn.gameObject, pointer, ExecuteEvents.submitHandler);
    }

    private void HandleMouse()
    {
        pointSelectedImages.ForEach(o => Destroy(o));
        pointSelectedImages.Clear();

        RaycastHit hit;

        if (!UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject(-1)
            && Physics.Raycast(GetComponent<Camera>().ScreenPointToRay(Input.mousePosition), out hit))
        {
            highlightedTile = hit.collider.gameObject.GetComponent<TileScript>();

            if (highlightedTile)
            {
                // Find out if changing altitude
                bool altitudeChange = playerTileAction == PlayerTileAction.Up || playerTileAction == PlayerTileAction.Down;

                if (Input.GetMouseButton(0))
                {
                    // For changing tile biome
                    if (!altitudeChange)
                    {
                        if (tileMouseDownAction != null && mouseDownTileLastFrame != highlightedTile)
                        {
                            tileMouseDownAction(highlightedTile);
                        }
                        mouseDownTileLastFrame = highlightedTile;
                    }
                }
                else
                {
                    // For changing tile altitude
                    if (Input.GetMouseButtonUp(0))
                    {
                        int manaCost = -1;
                        if (playerTileAction == PlayerTileAction.Up)
                            manaCost = gameManager.MovePointsUp(selectedPointsIndices, false);
                        else if (playerTileAction == PlayerTileAction.Down)
                            manaCost = gameManager.MovePointsDown(selectedPointsIndices, false);

                        if (manaCost > 0) PlayManaSpenAnim(manaCost);
                    }

                    selectedPointsIndices.Clear();
                }

                // Changing altitude
                if (altitudeChange)
                {
                    pointSelectedImages.ForEach(o => Destroy(o));
                    pointSelectedImages.Clear();
                    selectedPointsIndices.Clear();

                    Vector3 closest = highlightedTile.PointNW;

                    if (Vector3.Distance(highlightedTile.PointNE, hit.point) < Vector3.Distance(closest, hit.point))
                        closest = highlightedTile.PointNE;
                    if (Vector3.Distance(highlightedTile.PointSW, hit.point) < Vector3.Distance(closest, hit.point))
                        closest = highlightedTile.PointSW;
                    if (Vector3.Distance(highlightedTile.PointSE, hit.point) < Vector3.Distance(closest, hit.point))
                        closest = highlightedTile.PointSE;

                    // Remember the starting point or the only point (when left mouse button is up)
                    if (Input.GetMouseButtonDown(0) || !Input.GetMouseButton(0))
                    {
                        moveUpDownStartPointIndex.X = (Convert.ToInt32(closest.x) + 5) / 10;
                        moveUpDownStartPointIndex.Y = (Convert.ToInt32(closest.z) + 5) / 10;
                    }

                    // PERF: Could be only calculated when changed
                    int[,] altitudes = gameManager.Map.ComputeAltitudeMap();

                    Action<int, int> createTileSelectedImage = (x, y) =>
                    {
                        GameObject imageSelectedPoint = (GameObject)(UnityEngine.Object.Instantiate(Resources.Load<GameObject>("Prefabs/PointSelectedImage"), PointUpDownCanvas.transform, true));

                        imageSelectedPoint.transform.SetPositionAndRotation(new Vector3((x * 10) - 5, 9f + 2 * altitudes[x, y], (y * 10) - 5), PointUpDownCanvas.transform.rotation);
                        pointSelectedImages.Add(imageSelectedPoint);
                    };

                    int endPointIndexX = (Convert.ToInt32(closest.x) + 5) / 10;
                    int endPointIndexY = (Convert.ToInt32(closest.z) + 5) / 10;

                    int deltaX = endPointIndexX == moveUpDownStartPointIndex.X ? 1 : Math.Sign(endPointIndexX - moveUpDownStartPointIndex.X);
                    int deltaY = endPointIndexY == moveUpDownStartPointIndex.Y ? 1 : Math.Sign(endPointIndexY - moveUpDownStartPointIndex.Y);

                    if (moveUpDownStartPointIndex.X != -1 && moveUpDownStartPointIndex.Y != -1)
                    {
                        for (int y = moveUpDownStartPointIndex.Y; y != endPointIndexY + deltaY; y += deltaY)
                        {
                            for (int x = moveUpDownStartPointIndex.X; x != endPointIndexX + deltaX; x += deltaX)
                            {
                                createTileSelectedImage(x, y);
                                selectedPointsIndices.Add(new Point(x, y));
                            }
                        }
                    }
                }
                // Only highlight when not dealing with points
                else
                {
                    highlightedTile.ApplyShader("Custom/Highlight");
                }
            }
        }
        else
        {
            highlightedTile = null;

            selectedPointsIndices.Clear();
            if (!Input.GetMouseButton(0))
            {
                moveUpDownStartPointIndex.X = -1;
                moveUpDownStartPointIndex.Y = -1;
            }
        }

        if (Input.GetMouseButtonUp(0))
            mouseDownTileLastFrame = null;
    }
}