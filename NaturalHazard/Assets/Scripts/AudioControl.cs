﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Classes;
using UnityEngine;

public class AudioControl : MonoBehaviour
{
    public const string Tag = "AudioControl";

    public AudioSource music;
    public AudioSource sfx;

    public float MusicVolume
    {
        get { return music.volume; }
        set
        {
            music.volume = value;
            initialMusicVolume = value;
        }
    }
    public float SoundVolume
    {
        get { return sfx.volume; }
        set
        {
            sfx.volume = value;
            initialSfxVolume = value;
        }
    }

    public List<AudioClip> BuyTileSounds;
    public List<AudioClip> TileChangeSounds;
    public List<AudioClip> MoveTileUpSounds;
    public List<AudioClip> MoveTileDownSounds;
    public List<AudioClip> WinSounds;
    public List<AudioClip> LoseSounds;
    public List<AudioClip> ErrorSounds;

    public AudioClip MenuClick1;
    public AudioClip MenuClickIntense1;

    enum Fade { In, Out }
    
    public bool Playing { get { return music != null && music.isPlaying; } }
    private float initialMusicVolume;
    private float initialSfxVolume;

    public static AudioControl AudioInCurrentScene
    {
        get
        {
            var audioObject = GameObject.FindWithTag(AudioControl.Tag);
            if (audioObject)
                return audioObject.GetComponent<AudioControl>();
            else
                return null;
        }
    }

    void Awake()
    {
        initialMusicVolume = music.volume;
        initialSfxVolume = sfx.volume;
        this.tag = Tag;
    }

    public void StartMusic(float fadeTime)
    {
        if (!music.isPlaying)
            music.Play();
        StartCoroutine(FadeAudio(music, initialMusicVolume, fadeTime, Fade.In));
    }

    public void StopMusic(float fadeTime)
    {
        StartCoroutine(FadeOutAndStop(music, initialMusicVolume, fadeTime));
    }

    public void PlayBuySound()
    {
        PlayRandomSound(BuyTileSounds);
    }

    public void PlayChangeSound()
    {
        PlayRandomSound(TileChangeSounds);
    }

    public void PlayMenuClick()
    {
        PlaySound(MenuClick1);
    }

    public void PlayMenuClickIntense()
    {
        PlaySound(MenuClickIntense1);
    }

    public void PlayMoveTileUp()
    {
        PlayRandomSound(MoveTileUpSounds);
    }

    public void PlayMoveTileDown()
    {
        PlayRandomSound(MoveTileDownSounds);
    }

    public void PlayMeepma()
    {
        StartCoroutine(TransitionIntoNewClip(sfx, initialSfxVolume, 0.1f, ErrorSounds.RandomElement()));
    }

    public void PlayWinSound()
    {
        PlayRandomSound(WinSounds);
    }

    public void PlayLoseSound()
    {
        PlayRandomSound(LoseSounds);
    }

    public void PlaySound(AudioClip sound)
    {
        sfx.PlayOneShot(sound);
    }

    private void PlayRandomSound(IEnumerable<AudioClip> sounds)
    {
        if (sounds.Any())
            PlaySound(sounds.RandomElement());
    }

    private IEnumerator TransitionIntoNewClip(AudioSource source, float initialVolume, float fadeTime, AudioClip newClip)
    {
        yield return StartCoroutine(FadeOutAndStop(source, initialVolume, fadeTime / 2));
        source.clip = newClip;
        source.Play();
        yield return StartCoroutine(FadeAudio(source, initialVolume, fadeTime / 2, Fade.In));
    }

    private IEnumerator FadeOutAndStop(AudioSource source, float initialVolume, float fadeTime)
    {
        yield return StartCoroutine(FadeAudio(source, initialVolume, fadeTime, Fade.Out));
        source.Stop();
    }

    IEnumerator FadeAudio(AudioSource source, float initialVolume, float fadeTime, Fade fadeType)
    {
        float start = fadeType == Fade.In ? 0.0F : initialVolume;
        float end = fadeType == Fade.In ? initialVolume : 0.0F;
        float i = 0.0F;
        float step = 1.0F / fadeTime;

        while (i <= 1.0F)
        {
            i += step * Time.deltaTime;
            source.volume = Mathf.Lerp(start, end, i);
            yield return null;
        }
    }

    public void PauseMusic()
    {
        if (music.isPlaying)
            music.Pause();
    }

    internal void ResumeMusic()
    {
        if (!music.isPlaying)
            music.Play();
    }
}
