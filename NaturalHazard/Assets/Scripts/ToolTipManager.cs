﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ToolTipManager : MonoBehaviour
{
    public Text ForestToolTipPrizeValue;
    public Text RiverToolTipPrizeValue;
    public Text PlainToolTipPrizeValue;
    public Text MoveUpToolTipPrizeValue;
    public Text MoveDownToolTipPrizeValue;
    public Text BuyTileToolTipPrizeValue;

	void Start ()
	{
        ForestToolTipPrizeValue.text = Globals.GetBiomChangeManacost(TileType.None, TileType.Forest).ToString();
        RiverToolTipPrizeValue.text = Globals.GetBiomChangeManacost(TileType.None, TileType.River).ToString();
        PlainToolTipPrizeValue.text = Globals.GetBiomChangeManacost(TileType.None, TileType.Plain).ToString();

        MoveUpToolTipPrizeValue.text = Globals.PointMoveManacost.ToString();
        MoveDownToolTipPrizeValue.text = Globals.PointMoveManacost.ToString();

	    BuyTileToolTipPrizeValue.text = Globals.TileBuyManacost.ToString();
	}

	void Update ()
    {
	
	}
}
