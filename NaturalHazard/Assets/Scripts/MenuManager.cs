﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class MenuManager : MonoBehaviour
{
    public Menu MainMenu;
    public Menu CurrentMenu;
    private AudioControl Audio;

    public void Awake()
    {
        Audio = AudioControl.AudioInCurrentScene;
    }

    public void Start()
    {
        if (MainMenu != null)
            ShowMenu(MainMenu);
    }

    public void ShowMenu(Menu menu)
    {
        if (CurrentMenu != null)
        {
            CurrentMenu.IsOpen = false;
            Audio.PlayMenuClick();
        }

        CurrentMenu = menu;
        CurrentMenu.IsOpen = true;
    }

    public void ShowMenu(GameObject obj)
    {
        if (CurrentMenu != null)
        {
            CurrentMenu.IsOpen = false;
            Audio.PlayMenuClick();
        }

        var menu = obj.GetComponent<Menu>();

        CurrentMenu = menu;
        CurrentMenu.IsOpen = true;
    }

    public void ShowTooltip(GameObject obj)
    {
        var menu = obj.GetComponent<Menu>();

        menu.IsOpen = true;
    }

    public void HideTooltip(GameObject obj)
    {
        var menu = obj.GetComponent<Menu>();

        menu.IsOpen = false;
    }

    public void ExitApplication()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
    }

    public void PlaySandbox()
    {
        PlayerPrefs.SetString("CurrentLevel", Globals.SandboxMapName);
        var obj = FindObjectOfType<GameManager>();
        if (obj != null)
            obj.BailTheFuckOut();
        Application.LoadLevel("Game");
        Audio.PlayMenuClickIntense();
    }

    public void NewGame()
    {
        PlayerPrefs.SetString("CurrentLevel", Globals.MapNames[0]);
        var obj = FindObjectOfType<GameManager>();
        if (obj != null)
            obj.BailTheFuckOut();
        Application.LoadLevel("Game");
        Audio.PlayMenuClickIntense();
    }

    public void NextLevel()
    {
        string currentLevelName = PlayerPrefs.GetString("CurrentLevel");
        if (Globals.MapNames.Contains(currentLevelName) && Globals.MapNames.Last() != currentLevelName)
        {
            for (int i = 0; i < Globals.MapNames.Length; i++)
            {
                if (currentLevelName == Globals.MapNames[i] && i + 1 < Globals.MapNames.Length)
                {
                    PlayerPrefs.SetString("CurrentLevel", Globals.MapNames[i + 1]);
                    var obj = FindObjectOfType<GameManager>();
                    if (obj != null)
                        obj.BailTheFuckOut();
                    Audio.PlayMenuClickIntense();
                    Application.LoadLevel("Game");
                }
            }
        }
        else
        {
            RestartLevel();
        }
    }

    public void RestartLevel()
    {
        var obj = FindObjectOfType<GameManager>();
        if (obj != null)
            obj.BailTheFuckOut();
        Audio.PlayMenuClickIntense();
        Application.LoadLevel("Game");
    }

    public void BackToManu()
    {
        var obj = FindObjectOfType<GameManager>();
        if (obj != null)
            obj.BailTheFuckOut();
        Audio.PlayMenuClickIntense();
        Application.LoadLevel("Main Menu");
    }
}
