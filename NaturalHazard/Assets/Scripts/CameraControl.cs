﻿using UnityEngine;

/// <summary>
/// Component to be added to a transform one level above camera to allow controlling it with keyboard, mouse movement with specific button pressed or placing mouse cursor at the edge of screen.
/// </summary>
public class CameraControl : MonoBehaviour
{
    [Tooltip("Controls smoothing of zooming. Set lower values for presentations, because it looks cool.")]
    [Range(0.1f, 1)]
    public float ZoomAcceleration = 0.4f;

    [Tooltip("Defines size of zoom step (in meters).")]
    [Range(0, 100)]
    public float ZoomSensitivity = 10f;

    [Tooltip("The nearest to orbit center camera can go.")]
    public float MinDistance = 10;

    [Tooltip("The furthest to orbit center camera can go.")]
    public float MaxDistance = 200;

    private Vector3 Top { get { return Quaternion.AngleAxis(-transform.eulerAngles.x, transform.right) * transform.forward; } }
    private Vector3 Left { get { return Quaternion.Euler(0, -90, 0) * Top; } }
    private Vector3 Right { get { return Quaternion.Euler(0, 90, 0) * Top; } }
    private Vector3 Bottom { get { return Quaternion.Euler(0, 180, 0) * Top; } }
    private Vector3 initialPosition;
    private float initialZoom;
    private float moveSpeed = 2;
    private Vector3 previousMousePosition;

    private Transform mainCamera;
    private Plane lowerPlane;
    private float targetZoom;
    private Vector3 targetScreenPoint;
    private float horizontal, vertical, rotate, move, center, zoom;

    private GameManager gameManager;

    private Vector3 screenCenter
    {
        get { return new Vector3(Screen.width / 2, Screen.height / 2); }
    }

    private float currentZoom
    {
        get { return mainCamera.transform.localPosition.z; }
        set { mainCamera.transform.localPosition = new Vector3(0, 0, value); }
    }

    private void Start()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        mainCamera = transform.Find("Main Camera");
        initialPosition = transform.position;
        initialZoom = currentZoom;
        targetZoom = initialZoom;
        lowerPlane = new Plane(Vector3.up, 0);
    }

    private void Update()
    {
        if (gameManager.GameState == GameState.Paused) return;
        horizontal = Input.GetAxis("Pan camera horizontal");
        vertical = Input.GetAxis("Pan camera vertical");
        center = Input.GetAxis("Center camera");
        rotate = Input.GetAxis("Rotate camera");
        move = Input.GetAxis("Grab camera");
        zoom = Input.GetAxis("Zoom camera");

        // Keyboard
        if (vertical == 1f)
            transform.Translate(Top * moveSpeed, Space.World);
        if (horizontal == -1f)
            transform.Translate(Left * moveSpeed, Space.World);
        if (horizontal == 1f)
            transform.Translate(Right * moveSpeed, Space.World);
        if (vertical == -1f)
            transform.Translate(Bottom * moveSpeed, Space.World);
        if (center == 1f)
        {
            transform.position = initialPosition;
        }

        // Mouse moving
        if (move == 1f)
        {
            Cursor.visible = false;
            Vector3 previousPoint = ScreenPointToPlanePoint(previousMousePosition, lowerPlane);
            Vector3 currentPoint = ScreenPointToPlanePoint(Input.mousePosition, lowerPlane);
            transform.Translate(previousPoint - currentPoint, Space.World);
        }
        previousMousePosition = Input.mousePosition;

        // Mouse rotation
        if (rotate == 1f)
        {
            float dx = Input.GetAxis("Mouse X");
            transform.Rotate(Vector3.up * dx * 10, Space.World);
        }

        // Screen edges
        if (move != 1f && rotate != 1)
        {
            var x = Input.mousePosition.x;
            var y = Input.mousePosition.y;
            bool topEdge = y > Screen.height - 5 && y < Screen.height;
            bool leftEdge = x < 5 && x >= 0;
            bool rightEdge = x > Screen.width - 5 && x < Screen.width;
            bool bottomEdge = y < 5 && y >= 0;
            Vector3 v = Vector3.zero;
            if (topEdge) v += Top;
            if (leftEdge) v += Left;
            if (rightEdge) v += Right;
            if (bottomEdge) v += Bottom;
            transform.Translate(Vector3.Normalize(v) * moveSpeed, Space.World);
        }

        // Wheel zoom
        if (zoom != 0)
        {
            Zoom(zoom);
            targetScreenPoint = Input.mousePosition;
        }

        // Zoom smoothing & zoom to cursor
        if (Mathf.Abs(currentZoom - targetZoom) > 0.0001)
        {
            Vector3 previousPoint = ScreenPointToPlanePoint(targetScreenPoint, lowerPlane);
            currentZoom = Mathf.Lerp(currentZoom, targetZoom, Time.unscaledDeltaTime * ZoomAcceleration * 50);
            Vector3 currentPoint = ScreenPointToPlanePoint(targetScreenPoint, lowerPlane);
            transform.Translate(previousPoint - currentPoint, Space.World);
        }
    }

    private void LateUpdate()
    {
        if (Input.GetAxis("Grab camera") != 1f)
            Cursor.visible = true;
    }

    private void Zoom(float scrollAmount)
    {
        targetZoom = Mathf.Clamp(targetZoom + scrollAmount * ZoomSensitivity * 10, -MaxDistance, -MinDistance);
        //Debug.Log("CurrentZoom:\t" + currentZoom + "\nTargetZoom:\t\t" + targetZoom);
    }

    private Vector3 ScreenPointToPlanePoint(Vector3 screenPoint, Plane plane)
    {
        Ray ray = mainCamera.GetComponent<Camera>().ScreenPointToRay(screenPoint);
        float distance;
        plane.Raycast(ray, out distance);
        return ray.GetPoint(distance);
    }
}