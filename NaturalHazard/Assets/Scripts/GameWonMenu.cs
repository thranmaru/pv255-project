﻿using UnityEngine;
using System.Collections;
using System.Linq;
using UnityEngine.UI;

public class GameWonMenu : MonoBehaviour
{
    public Button NextLevelButton;

    void Start()
    {
        string currentLevelName = PlayerPrefs.GetString("CurrentLevel");
        if (Globals.MapNames.Last() == currentLevelName)
        {
            NextLevelButton.gameObject.SetActive(false);
        }
        else
        {
            NextLevelButton.gameObject.SetActive(true);
        }
    }
}
