﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

#if UNITY_EDITOR
using UnityEditor;
#endif

enum GUITileAction { BuyTile, SellTile, Forest, Plain, River, City, Up, AddDisSpa, DelDisSpa, Fire, Blizzard, Swamp, Desert, Down }

public class EditorGUI : MonoBehaviour
{
    public Transform GameManagerTransform;
    public GUISkin Skin;
    public Canvas Canvas;

    public int SelectedMapViewIndex;
    public int SelectedDisasterSpawnIndex = 0;

    private GameManager gameManager;

    // For changing biome
    private TileScript highlightedTile;
    private TileScript mouseDownTileLastFrame;

    // For changing altitude
    private List<GameObject> pointSelectedImages = new List<GameObject>();
    private List<GameObject> disasterSpawnTilesImages = new List<GameObject>();
    private List<Point> selectedPointsIndices = new List<Point>();

    private Action<TileScript> tileMouseDownAction;
    private int selectedTileActionIndex = -1;

    // Rectangles where GUI is
    private Rect mapViewSelectionRect = new Rect(0f, (Screen.height / 2), 110f, 100f);
    private Rect tileActionSelectionRect = new Rect(0f, Screen.height - 90f, 600f, 90f);
    private Rect menuRect = new Rect(Screen.width - 110f, 0f, 110f, 120f);
    private Rect mapParamsRect = new Rect(0f, 0f, 140f, 260f);

    private Point moveUpDownStartPointIndex = new Point(-1, -1);

    private string[] viewTypeNames = new[] { "Normal", "Heat Map", "Humidity Map" };
    private string[] disasterNames = new[] { "Fire", "Blizzard", "Desert", "Swamp" };

    void Start()
    {
        gameManager = GameManagerTransform.GetComponent<GameManager>();
    }

    void Update()
    {
        HandleKeyboard();
        HandleMouse();

        disasterSpawnTilesImages.ForEach(o => Destroy(o));
        disasterSpawnTilesImages.Clear();
        foreach (Point p in gameManager.Map.Scenario.SpawningTilesIndices)
        {
            GameObject disasterTileImage = (GameObject)(UnityEngine.Object.Instantiate(Resources.Load<GameObject>("Prefabs/DisaterSpawnImage"),
                            gameManager.Map.GetTile(p.X, p.Y).transform.position + new Vector3(0f, 11f, 0f), Canvas.transform.rotation));

            disasterTileImage.transform.SetParent(Canvas.transform);
            disasterSpawnTilesImages.Add(disasterTileImage);
        }
    }

    private void OnGUI()
    {
        GUI.skin = Skin;

        // Menu
        GUI.Box(menuRect, "Menu");
        if (GUI.Button(new Rect(menuRect.x + 5f, menuRect.y + 20f, 100f, 25f), "Play"))
        {
            disasterSpawnTilesImages.ForEach(o => Destroy(o));
            disasterSpawnTilesImages.Clear();

            gameManager.ChangeGameState(GameState.Playing);
            GetComponentInParent<GameGUI>().enabled = true;
            enabled = false;
        }
#if UNITY_EDITOR
        if (GUI.Button(new Rect(menuRect.x + 5f, menuRect.y + 45f, 100f, 25f), "Load map"))
        {
            string path = EditorUtility.OpenFilePanel("Select map file", "Assets/Resources/Maps", "txt");
            if (path.Length != 0)
                LoadMap(path);
            SelectedDisasterSpawnIndex = Globals.TileTypeToDisasterIndex(gameManager.Map.Scenario.DisasterType);
        }
        if (GUI.Button(new Rect(menuRect.x + 5f, menuRect.y + 70, 100f, 25f), "Save map"))
        {
            string path = EditorUtility.SaveFilePanel("Save map file", "Assets/Resources/Maps", "newMap", "txt");
            if (path.Length != 0)
                SaveMap(path);
        }
#endif
        if (GUI.Button(new Rect(menuRect.x + 5f, menuRect.y + 95f, 100f, 25f), "Exit"))
        {
#if UNITY_EDITOR
            EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        }

        // Map parameters
        GUI.Box(mapParamsRect, "Map parameters");
        GUI.Label(new Rect(mapParamsRect.x + 5f, mapParamsRect.y + 25f, 60f, 25f), "Size X:");
        int value;
        if (int.TryParse(GUI.TextField(new Rect(mapParamsRect.x + 90f, mapParamsRect.y + 25f, 40f, 20f), gameManager.SizeX.ToString()), out value))
            gameManager.SizeX = value;
        GUI.Label(new Rect(mapParamsRect.x + 5f, mapParamsRect.y + 50f, 60f, 25f), "Size Y:");
        if (int.TryParse(GUI.TextField(new Rect(mapParamsRect.x + 90f, mapParamsRect.y + 50f, 40f, 20f), gameManager.SizeY.ToString()), out value))
            gameManager.SizeY = value;
        GUI.Label(new Rect(mapParamsRect.x + 5f, mapParamsRect.y + 75f, 60f, 55f), "Starting mana:");
        if (int.TryParse(GUI.TextField(new Rect(mapParamsRect.x + 90f, mapParamsRect.y + 80f, 40f, 20f), gameManager.Map.Scenario.StartingMana.ToString()), out value))
            gameManager.Map.Scenario.StartingMana = value;
        GUI.Label(new Rect(mapParamsRect.x + 5f, mapParamsRect.y + 110f, 60f, 55f), "Disaster spawn time:");
        if (int.TryParse(GUI.TextField(new Rect(mapParamsRect.x + 90f, mapParamsRect.y + 125f, 40f, 20f), gameManager.Map.Scenario.SpawningTime.ToString()), out value))
            gameManager.Map.Scenario.SpawningTime = value;
        GUI.Label(new Rect(mapParamsRect.x + 5f, mapParamsRect.y + 160f, 120f, 55f), "Disaster spawn tiles count: " + gameManager.Map.Scenario.SpawningTilesIndices.Count);

        SelectedDisasterSpawnIndex = GUI.SelectionGrid(new Rect(mapParamsRect.x + 5f, mapParamsRect.y + 200f, mapParamsRect.width - 10f, 60f), SelectedDisasterSpawnIndex, disasterNames, 2);
        gameManager.Map.Scenario.DisasterType = DisasterIndexToTileType(SelectedDisasterSpawnIndex);

        // Map visualization
        GUI.Box(mapViewSelectionRect, "");
        // Check if map view changed so ChangeMapView isn't called every frame unnecessarily
        int oldMapViewIndex = SelectedMapViewIndex;
        SelectedMapViewIndex = GUI.SelectionGrid(new Rect(mapViewSelectionRect), SelectedMapViewIndex, viewTypeNames, 1);
        if (SelectedMapViewIndex != oldMapViewIndex)
        {
            switch (SelectedMapViewIndex)
            {
                case 0:
                    gameManager.ChangeMapView(MapView.Normal);
                    break;
                case 1:
                    gameManager.ChangeMapView(MapView.Temperature);
                    break;
                case 2:
                    gameManager.ChangeMapView(MapView.Humidity);
                    break;
            }
        }

        // Placing tiles
        GUI.Box(tileActionSelectionRect, "Change Tiles");
        selectedTileActionIndex = GUI.SelectionGrid(new Rect(tileActionSelectionRect.x + 5f, tileActionSelectionRect.y + 20f, tileActionSelectionRect.width - 10f, 60f), selectedTileActionIndex, Enum.GetNames(typeof(GUITileAction)), Enum.GetNames(typeof(GUITileAction)).Length / 2);

        if ((GUITileAction)selectedTileActionIndex == GUITileAction.AddDisSpa)
            tileMouseDownAction = (t) => gameManager.AddDistasterSpawn(t);
        else if ((GUITileAction)selectedTileActionIndex == GUITileAction.DelDisSpa)
            tileMouseDownAction = (t) => gameManager.RemoveDisasterSpawn(t);
        else if (selectedTileActionIndex > 1)
            tileMouseDownAction = (t) => gameManager.ChangeTileType(t, (TileType)Enum.Parse(typeof(TileType), ((GUITileAction)selectedTileActionIndex).ToString()));
        // Buy tile
        else if ((GUITileAction)selectedTileActionIndex == GUITileAction.BuyTile)
            tileMouseDownAction = (t) => gameManager.BuyTile(t);
        // Sell tile
        else if ((GUITileAction)selectedTileActionIndex == GUITileAction.SellTile)
            tileMouseDownAction = (t) => gameManager.SellTile(t);

        // Tile info
        if (highlightedTile)
        {
            Rect boxRect = new Rect(Screen.width - 200f, Screen.height - 150f, 200f, 150f);
            GUI.Box(boxRect, "Tile Info");
            GUI.Label(new Rect(boxRect.x + 5f, boxRect.y + 20f, boxRect.width - 10f, 25f), "Tile type: " + highlightedTile.TileData.TileType);
            GUI.Label(new Rect(boxRect.x + 5f, boxRect.y + 40f, boxRect.width - 10f, 25f), "Humidity: " + highlightedTile.TileData.Humidity);
            GUI.Label(new Rect(boxRect.x + 5f, boxRect.y + 60f, boxRect.width - 10f, 25f), "Temperature: " + highlightedTile.TileData.Temperature);
            GUI.Label(new Rect(boxRect.x + 5f, boxRect.y + 80f, boxRect.width - 10f, 25f), "Altitude: " + highlightedTile.TileData.Altitude);
            GUI.Label(new Rect(boxRect.x + 5f, boxRect.y + 100f, boxRect.width - 10f, 25f), "NW: " + highlightedTile.TileData.AltitudeNW + "   NE: " + highlightedTile.TileData.AltitudeNE +
                "   SW: " + highlightedTile.TileData.AltitudeSW + "   SE:" + highlightedTile.TileData.AltitudeSE);
            GUI.Label(new Rect(boxRect.x + 5f, boxRect.y + 120f, boxRect.width - 10f, 25f), "Grid position:  X:" + highlightedTile.TileData.GridPosition.x +
                "  Y:" + highlightedTile.TileData.GridPosition.y);
        }
    }

    private void HandleKeyboard()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            tileMouseDownAction = null;
    }

    private void HandleMouse()
    {
        pointSelectedImages.ForEach(o => Destroy(o));
        pointSelectedImages.Clear();

        RaycastHit hit;
        Vector2 mousePositionInvertedY = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);

        if (!tileActionSelectionRect.Contains(mousePositionInvertedY) && !mapViewSelectionRect.Contains(mousePositionInvertedY) && !menuRect.Contains(mousePositionInvertedY) &&
            !mapParamsRect.Contains(mousePositionInvertedY) && Physics.Raycast(GetComponent<Camera>().ScreenPointToRay(Input.mousePosition), out hit))
        {
            highlightedTile = hit.collider.gameObject.GetComponent<TileScript>();

            if (highlightedTile)
            {
                // Find out if changing altitude
                bool altitudeChange = (GUITileAction)selectedTileActionIndex == GUITileAction.Up || (GUITileAction)selectedTileActionIndex == GUITileAction.Down;

                if (Input.GetMouseButton(0))
                {
                    // For changing tile biome
                    if (!altitudeChange)
                    {
                        if (tileMouseDownAction != null && mouseDownTileLastFrame != highlightedTile)
                        {
                            tileMouseDownAction(highlightedTile);
                        }
                        mouseDownTileLastFrame = highlightedTile;
                    }
                }
                else
                {
                    // For changing tile altitude
                    if (Input.GetMouseButtonUp(0))
                    {
                        if ((GUITileAction)selectedTileActionIndex == GUITileAction.Up)
                            gameManager.MovePointsUp(selectedPointsIndices, true);
                        else if ((GUITileAction)selectedTileActionIndex == GUITileAction.Down)
                            gameManager.MovePointsDown(selectedPointsIndices, true);
                    }

                    selectedPointsIndices.Clear();
                }

                // Changing altitude
                if (altitudeChange)
                {
                    pointSelectedImages.ForEach(o => Destroy(o));
                    pointSelectedImages.Clear();
                    selectedPointsIndices.Clear();

                    Vector3 closest = highlightedTile.PointNW;

                    if (Vector3.Distance(highlightedTile.PointNE, hit.point) < Vector3.Distance(closest, hit.point))
                        closest = highlightedTile.PointNE;
                    if (Vector3.Distance(highlightedTile.PointSW, hit.point) < Vector3.Distance(closest, hit.point))
                        closest = highlightedTile.PointSW;
                    if (Vector3.Distance(highlightedTile.PointSE, hit.point) < Vector3.Distance(closest, hit.point))
                        closest = highlightedTile.PointSE;

                    // Remember the starting point or the only point (when left mouse button is up)
                    if ((Input.GetMouseButtonDown(0) || !Input.GetMouseButton(0)))
                    {
                        moveUpDownStartPointIndex.X = (Convert.ToInt32(closest.x) + 5) / 10;
                        moveUpDownStartPointIndex.Y = (Convert.ToInt32(closest.z) + 5) / 10;
                    }

                    // PERF: Could be only calculated when changed
                    int[,] altitudes = gameManager.Map.ComputeAltitudeMap();

                    Action<int, int> createTileSelectedImage = (x, y) =>
                    {
                        GameObject imageSelectedPoint = (GameObject)(UnityEngine.Object.Instantiate(Resources.Load<GameObject>("Prefabs/PointSelectedImage"), Canvas.transform, true));

                        imageSelectedPoint.transform.SetPositionAndRotation(new Vector3((x * 10) - 5, 9f + 2 * altitudes[x, y], (y * 10) - 5), Canvas.transform.rotation);
                        pointSelectedImages.Add(imageSelectedPoint);
                    };

                    int endPointIndexX = (Convert.ToInt32(closest.x) + 5) / 10;
                    int endPointIndexY = (Convert.ToInt32(closest.z) + 5) / 10;

                    int deltaX = endPointIndexX == moveUpDownStartPointIndex.X ? 1 : Math.Sign(endPointIndexX - moveUpDownStartPointIndex.X);
                    int deltaY = endPointIndexY == moveUpDownStartPointIndex.Y ? 1 : Math.Sign(endPointIndexY - moveUpDownStartPointIndex.Y);

                    if (moveUpDownStartPointIndex.X != -1 && moveUpDownStartPointIndex.Y != -1)
                    {
                        for (int y = moveUpDownStartPointIndex.Y; y != endPointIndexY + deltaY; y += deltaY)
                        {
                            for (int x = moveUpDownStartPointIndex.X; x != endPointIndexX + deltaX; x += deltaX)
                            {
                                createTileSelectedImage(x, y);
                                selectedPointsIndices.Add(new Point(x, y));
                            }
                        }
                    }
                }
                // Only highlight when not dealing with points
                else
                {
                    highlightedTile.ApplyShader("Custom/Highlight");
                }
            }
        }
        else
        {
            highlightedTile = null;

            selectedPointsIndices.Clear();
            if (!Input.GetMouseButton(0))
            {
                moveUpDownStartPointIndex.X = -1;
                moveUpDownStartPointIndex.Y = -1;
            }
        }

        if (Input.GetMouseButtonUp(0))
            mouseDownTileLastFrame = null;

        if (Input.GetMouseButtonDown(1))
        {
            tileMouseDownAction = null;
            selectedTileActionIndex = -1;
            selectedPointsIndices.Clear();
        }
    }

    private TileType DisasterIndexToTileType(int selectedDisasterSpawnIndex)
    {
        switch (selectedDisasterSpawnIndex)
        {
            case 0:
                return TileType.Fire;
            case 1:
                return TileType.Blizzard;
            case 2:
                return TileType.Desert;
            case 3:
                return TileType.Swamp;
            default:
                return TileType.Fire;
        }
    }

    private void LoadMap(string path)
    {
        gameManager.LoadMapFromFile(path);
    }

    private void SaveMap(string path)
    {
        gameManager.SaveMap(path);
    }
}