﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using UnityEngine;

public enum MapView
{
    Normal,
    Temperature,
    Humidity
}

public enum GameState
{
    Editor,
    Playing,
    Paused
}

public class GameManager : MonoBehaviour
{
    public Transform GameField;
    public static System.Random rng = new System.Random(); // Cross-thread safe rng

    private AudioControl Audio;
    private GameState gameState = GameState.Editor;
    private Map map;
    private System.Timers.Timer gameFlowTimer;
    private object lockObject = new object();
    private object bufferLock = new object();
    private System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
    private bool bufferChanged = false;
    private int iterations = 0;
    private GameObject gameGUICanvas;
    public static float currentTime;
    private bool endGameScreenShowed = false;

    public GameState GameState
    {
        get { return gameState; }
    }

    public Map Map
    {
        get { return map; }
    }

    private void Awake()
    {
        map = new Map(GameField);
        this.gameGUICanvas = GameObject.FindGameObjectWithTag("GameGUICanvas");
        if (this.gameGUICanvas == null)
            this.gameGUICanvas = GameObject.FindGameObjectWithTag("GameGUITransform").transform.GetChild(0).gameObject;
        Audio = AudioControl.AudioInCurrentScene;
    }

    private void Start()
    {
        int audioOn = PlayerPrefs.GetInt("AudioOn", 1);
        if (audioOn == 0)
        {
            Audio.SoundVolume = 0;
            Audio.MusicVolume = 0;
        }
        else
        {
            Audio.SoundVolume = PlayerPrefs.GetFloat("EffectsVolume", 1f);
            Audio.MusicVolume = PlayerPrefs.GetFloat("MusicVolume", 1f);
        }

        map.LoadMapFromResource(PlayerPrefs.GetString("CurrentLevel", Globals.MapNames[0]));
        LoadMapToBuffer();

#if UNITY_EDITOR
        ChangeGameState(GameState.Editor);
        Camera.main.GetComponent<EditorGUI>().enabled = true;
        Camera.main.GetComponent<EditorGUI>().SelectedDisasterSpawnIndex = Globals.TileTypeToDisasterIndex(map.Scenario.DisasterType);
        Camera.main.GetComponent<GameGUI>().enabled = false;
#else
        ChangeGameState(GameState.Playing);
        Camera.main.GetComponent<EditorGUI>().enabled = false;
        Camera.main.GetComponent<GameGUI>().enabled = true;
#endif
    }

    private void Update()
    {
        if (Application.isPlaying)
        {
            if (!endGameScreenShowed && (map.Scenario.ScenarioState == ScenarioState.CityUndamaged || map.Scenario.ScenarioState == ScenarioState.CityDamaged) && (map.Scenario.DisasterType != TileType.None))
            {
                Audio.StopMusic(0.25f);
                GameObject overlay = gameGUICanvas.transform.GetChild(12).gameObject;
                overlay.SetActive(true);
                overlay.GetComponent<Animator>().Play("Lost Menu Start");
                Audio.PlayWinSound();
                endGameScreenShowed = true;
            }
            else if (!endGameScreenShowed && map.Scenario.ScenarioState == ScenarioState.CityDestroyed)
            {
                Audio.StopMusic(0.25f);
                GameObject overlay = gameGUICanvas.transform.GetChild(11).gameObject;
                overlay.SetActive(true);
                overlay.GetComponent<Animator>().Play("Lost Menu Start");
                Audio.PlayLoseSound();
                endGameScreenShowed = true;
            }
            currentTime = Time.time;
            if (bufferChanged)
            {
                lock (bufferLock)
                {
                    map.UpdateMapFromBuffer(TileInteraction.DataBuffer);
                    bufferChanged = false;
                    Monitor.Pulse(bufferLock);
                }
            }
            map.UpdateMapView();
            map.UpdateMapViewValues();
        }
    }

    public void ChangeGameState(GameState state)
    {
        GameState previous = gameState;
        gameState = state;
        if (gameState == GameState.Playing && previous != GameState.Paused)
        {
            TileInteraction.Reset();
            gameFlowTimer = new System.Timers.Timer(100) { AutoReset = true };
            gameFlowTimer.Elapsed += gameFlowTimer_Elapsed;
            gameFlowTimer.Start();
            Audio.StartMusic(3);
            Mana = Map.Scenario.StartingMana;
            Map.Scenario.GameStartTime = Time.time;
            Map.Scenario.DisasterSpawned = false;
            if (Map.Scenario.CityTilesCount == 0)
                Map.Scenario.CityTilesCount = Map.Tiles.Count(t => t.TileData.TileType == TileType.City);
            gameGUICanvas.SetActive(true);

        }
        if (gameState == GameState.Playing && previous == GameState.Paused)
        {
            gameFlowTimer.Start();
            Audio.ResumeMusic();
        }
        else if (gameState == GameState.Editor)
        {
            if (gameFlowTimer != null)
                gameFlowTimer.Stop();
            if (Audio.Playing)
                Audio.StopMusic(0.25f);
            gameGUICanvas.SetActive(false);

        }
        else if (gameState == GameState.Paused)
        {
            if (gameFlowTimer != null)
                gameFlowTimer.Stop();
            Audio.PauseMusic();
        }
    }

    void OnDestroy()
    {
        BailTheFuckOut();
    }

    public void OnApplicationQuit()
    {
        BailTheFuckOut();
    }    

    public void BailTheFuckOut()
    {
        Debug.Log("Releasing all threading stuff");
        if (gameFlowTimer != null)
        {
            gameFlowTimer.Stop();
            gameFlowTimer.Elapsed -= gameFlowTimer_Elapsed;
        }
        bufferChanged = false;
        lock (bufferLock)
            Monitor.PulseAll(bufferLock);
    }

    void gameFlowTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
    {
        lock (lockObject)
        {
            try
            {
                lock (bufferLock)
                {
                    while (bufferChanged)
                    {
                        Monitor.Wait(bufferLock);
                    }
                }
                LoadMapToBuffer();
                sw.Reset();
                sw.Start();
                // Calculate tile interaction
                TileInteraction.CalculateTileTemperature();
                TileInteraction.CalculateHumidityFlow();
                TileInteraction.CalculateHumidityGeneration();
                TileInteraction.DisasterControl();
                if (map.Scenario != null)
                    TileInteraction.EvaluateScenario(map.Scenario, currentTime);
                Mana += ManaDelta = TileInteraction.CalculateManaGeneration();
                bufferChanged = true;
                sw.Stop();
                if (iterations == 10)
                {
                    Debug.Log(string.Format("Calculation done in :{0} ms", sw.ElapsedMilliseconds));
                    iterations = 0;
                }
                else
                {
                    iterations++;
                }
            }
            catch (System.Exception ex)
            {
                Debug.LogError(string.Format("Something went wrong inside tuna's blackbox: {0}", ex.StackTrace));
            }
        }
    }

    private void LoadMapToBuffer()
    {
        TileInteraction.DataBuffer = map.GetTileListCopy();
    }

    #region GUI Proxies
    internal int SizeX
    {
        get { return map == null ? 0 : map.SizeX; }
        set { if (map != null) map.SizeX = value; }
    }

    internal int SizeY
    {
        get { return map == null ? 0 : map.SizeY; }
        set { if (map != null) map.SizeY = value; }
    }

    internal float Mana { get; set; }

    internal float ManaDelta { get; set; }

    internal int MovePointsUp(List<Point> pointIndices, bool forceMove)
    {
        return MovePoints(pointIndices, 1, forceMove);
    }

    internal int MovePointsDown(List<Point> pointIndices, bool forceMove)
    {
        return MovePoints(pointIndices, -1, forceMove);
    }

    private int MovePoints(List<Point> pointIndices, int delta, bool forceMove)
    {
        int manaCost = -1;
        int movedTiles = map.MovePoints(pointIndices, delta, forceMove, Mana);
        if (movedTiles > 0 && !forceMove)
        {
            if (delta < 0)
                Audio.PlayMoveTileDown();
            else if (delta > 0)
                Audio.PlayMoveTileUp();
            manaCost = movedTiles * Globals.PointMoveManacost;
            Mana -= manaCost;
        }
        else if (movedTiles == -2)
        {
            Audio.PlayMeepma();
        }

        map.UpdateMapView();
        return manaCost;
    }

    internal void ChangeMapView(MapView mapView)
    {
        map.ChangeMapView(mapView);
    }

    internal int ChangeTileType(TileScript tile, TileType tileType)
    {
        if (tile.TileData.TileType == tileType || (tile.TileData.TileType == TileType.Lake && tileType == TileType.River))
            return -1;

        int manaCost = -1;
        if (gameState == GameState.Playing && tile.TileData.Owner == GameEntity.Player && Mana >= Globals.GetBiomChangeManacost(tile.TileData.TileType, tileType))
        {
            map.ChangeTileType(tile, tileType);
            tile.TileData.ChangeTimestamp = currentTime;
            map.UpdateMapView();
            manaCost = Globals.GetBiomChangeManacost(tile.TileData.TileType, tileType);
            Mana -= manaCost;
            Audio.PlayChangeSound();
        }
        else if (gameState == GameState.Editor || tileType.IsDisaster())
        {
            map.ChangeTileType(tile, tileType);
            map.UpdateMapView();
        }
        else
        {
            Audio.PlayMeepma();
        }
        return manaCost;
    }

    internal int BuyTile(TileScript tile)
    {
        int manaCost = -1;
        if (gameState == GameState.Playing && (tile.TileData.Owner == GameEntity.Nature || tile.TileData.IsDevastated()) && Mana >= Globals.TileBuyManacost && map.PlayerTileNearby(tile))
        {
            tile.TileData.Owner = GameEntity.Player;
            manaCost = Globals.TileBuyManacost;
            Mana -= manaCost;
            Audio.PlayBuySound();
        }
        else if (gameState == GameState.Editor)
        {
            tile.TileData.Owner = GameEntity.Player;
        }
        else if (tile.TileData.Owner != GameEntity.Player)
        {
            Audio.PlayMeepma();
        }
        return manaCost;
    }

    internal void SellTile(TileScript tile) // Only in editor
    {
        tile.TileData.Owner = GameEntity.Nature;
    }

    internal void AddDistasterSpawn(TileScript tile)
    {
        bool found = false;
        foreach (Point p in map.Scenario.SpawningTilesIndices)
        {
            if (tile.GridPositionX == p.X && tile.GridPositionY == p.Y)
            {
                found = true;
                break;
            }
        }

        if (!found)
            map.Scenario.SpawningTilesIndices.Add(new Point(tile.GridPositionX, tile.GridPositionY));
    }

    internal void RemoveDisasterSpawn(TileScript tile)
    {
        for (int i = 0; i < map.Scenario.SpawningTilesIndices.Count; i++)
        {
            if (tile.GridPositionX == map.Scenario.SpawningTilesIndices[i].X && tile.GridPositionY == map.Scenario.SpawningTilesIndices[i].Y)
                map.Scenario.SpawningTilesIndices.RemoveAt(i);
        }
    }

    internal void LoadMapFromFile(string path)
    {
        map.LoadMapFromFile(path);
    }

    internal void LoadMapFromResource(string path)
    {
        map.LoadMapFromResource(path);
    }

    internal void SaveMap(string path)
    {
        map.SaveMap(path);
    }
    #endregion
}