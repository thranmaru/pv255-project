﻿using UnityEngine;
using System.Collections;

public class PauseMenuScript : MonoBehaviour
{
    public GameObject pauseMenuPanel;
    public GameManager gameManager;
    private Animator anim;
    private bool isPaused = false;
    private bool pausedAxisInUse = false;

    void Start()
    {
        Time.timeScale = 1;

        anim = pauseMenuPanel.GetComponent<Animator>();
        anim.enabled = false;

        RectTransform rect = pauseMenuPanel.GetComponent<RectTransform>();
        rect.offsetMax = new Vector2(0, 0);
        rect.offsetMin = new Vector2(0, 0);
    }


    public void Update()
    {
        if (Input.GetAxisRaw("Pause") != 0)
        {
            if (Input.GetAxis("Pause") > 0f && !isPaused && pausedAxisInUse == false)
            {
                PauseGame();
                pausedAxisInUse = true;
            }
            else if (Input.GetAxis("Pause") > 0f && isPaused && pausedAxisInUse == false)
            {
                UnpauseGame();
                pausedAxisInUse = true;
            }
        }
        if (Input.GetAxisRaw("Pause") == 0)
        {
            pausedAxisInUse = false;
        }
    }

    public void PauseGame()
    {
        anim.enabled = true;
        anim.Play("Open");
        isPaused = true;
        gameManager.ChangeGameState(GameState.Paused);

        Time.timeScale = 0;
    }

    public void UnpauseGame()
    {
        isPaused = false;
        anim.Play("Closed");
        gameManager.ChangeGameState(GameState.Playing);

        Time.timeScale = 1;
    }

    public void ShowMenu(GameObject obj)
    {
        Menu menu = obj.GetComponent<Menu>();

        menu.IsOpen = true;
    }

    public void BackToMenu()
    {
        Application.LoadLevel("Main Menu");
        Time.timeScale = 1;
    }

}
