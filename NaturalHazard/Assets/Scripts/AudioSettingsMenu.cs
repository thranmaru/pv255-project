﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AudioSettingsMenu : MonoBehaviour
{
    private const string audioOnKey = "AudioOn";
    private const string musicVolumeKey = "MusicVolume";
    private const string effectsVolumeKey = "EffectsVolume";
    public Toggle MuteAudioToggle;
    public Slider MusicSlider;
    public Slider EffectsSlider;

    public int AudioOnValue;
    public float MusicVolume;
    public float EffectsVolume;

    private AudioControl Audio;

    public void Awake()
    {
        Audio = AudioControl.AudioInCurrentScene;

        if(!PlayerPrefs.HasKey(audioOnKey))
            PlayerPrefs.SetInt(audioOnKey, 1);
        AudioOnValue = PlayerPrefs.GetInt(audioOnKey);
        if (AudioOnValue == 1)
            MuteAudioToggle.isOn = false;
        else
            MuteAudioToggle.isOn = true;

        MusicVolume = PlayerPrefs.GetFloat(musicVolumeKey, 0.5f);
        MusicSlider.value = MusicVolume;

        EffectsVolume = PlayerPrefs.GetFloat(effectsVolumeKey, 1f);
        EffectsSlider.value = EffectsVolume;
    }

    public void Start()
    {
        ToggleAudio();
    }
    
    public void ToggleMute()
    {
        if (MuteAudioToggle.isOn)
            AudioOnValue = 0;
        else
            AudioOnValue = 1;

        ToggleAudio();
        PlayerPrefs.SetInt(audioOnKey, AudioOnValue);
    }

    public void MusicSliderValueChanged()
    {
        MusicVolume = MusicSlider.value;
        SetMusicVolume(MusicVolume);
    }

    public void EffectsSliderValueChanged()
    {
        EffectsVolume = EffectsSlider.value;
        SetEffectsVolume(EffectsVolume);
    }

    private void ToggleAudio()
    {
        SetMusicVolume(MusicVolume);
        SetEffectsVolume(EffectsVolume);
    }

    private void SetMusicVolume(float value)
    {
        if (AudioOnValue == 1)
            Audio.MusicVolume = value;
        else
            Audio.MusicVolume = 0;
        PlayerPrefs.SetFloat(musicVolumeKey, value);
    }

    private void SetEffectsVolume(float value)
    {
        if (AudioOnValue == 1)
            Audio.SoundVolume = value;
        else
            Audio.SoundVolume = 0;
        PlayerPrefs.SetFloat(effectsVolumeKey, EffectsVolume);
    }
}
