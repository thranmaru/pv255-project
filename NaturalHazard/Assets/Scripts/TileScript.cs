﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Random = UnityEngine.Random;


public class TileScript : MonoBehaviour
{
    private static readonly string[] buildings, desertProps, frozenProps, plainProps, forestProps, swampProps, witheredProps;
    public const int MinAltitude = -5;
    public const int MaxAltitude = 5;
    private const int propPlacementTryCount = 10;

    public List<GameObject> TerrainObjects { get; set; }

    public Tile TileData { get; set; }

    public TileScript()
    {
        TileData = new Tile();
        TerrainObjects = new List<GameObject>();
    }

    static TileScript()
    {
        buildings = new string[]
        {
            "Prefabs/BuildingModel1",
            "Prefabs/BuildingModel2",
            "Prefabs/BuildingModel4",
            "Prefabs/BuildingModel5"
        };
        desertProps = new string[]
        {
            "Prefabs/Props/Cactus",
            "Prefabs/Props/Cactus2",
            "Prefabs/Props/Rock",
            "Prefabs/Props/Rock",
            "Prefabs/Props/Rock",
            "Prefabs/Props/RockWithWheel"
        };
        frozenProps = new string[]
        {
            "Prefabs/Props/Snowman"
        };
        plainProps = new string[]
        {
            "Prefabs/Props/Sheep",
            "Prefabs/Props/Flower",
            "Prefabs/Props/Flower",
            "Prefabs/Props/Flower",
            "Prefabs/Props/Bush"
        };
        forestProps = new string[]
        {
            "Prefabs/Tree4LowQ",
            "Prefabs/Tree3"

        };
        swampProps = new string[]
        {
            "Prefabs/Props/SwampTree1",
            "Prefabs/Props/SwampTree2",
            "Prefabs/Props/SwampTree3",
            "Prefabs/Props/SwampTree4"
        };
        witheredProps = new string[]
        {
            "Prefabs/Props/Rock"
        };
    }

    public int GridPositionX
    {
        get { return (int)TileData.GridPosition.x; }
    }

    public int GridPositionY
    {
        get { return (int)TileData.GridPosition.y; }
    }

    public Vector3 PointNW
    {
        get { return transform.position + new Vector3(-5f, 8f, 5f); }
    }
    public Vector3 PointNE
    {
        get { return transform.position + new Vector3(5f, 8f, 5f); }
    }
    public Vector3 PointSW
    {
        get { return transform.position + new Vector3(-5f, 8f, -5f); }
    }
    public Vector3 PointSE
    {
        get { return transform.position + new Vector3(5f, 8f, -5f); }
    }

    public bool RenderTerrain
    {
        set
        {
            foreach (GameObject o in TerrainObjects)
                o.SetActive(value);
        }
    }

    internal void SetAltitude(int altitude)
    {
        TileData.Altitude = Mathf.Clamp(altitude, MinAltitude, MaxAltitude);
        transform.position = new Vector3(transform.position.x, TileData.Altitude * 2, transform.position.z);
    }

    internal void UpdateAltitude()
    {
        SetAltitude(Convert.ToInt32(Math.Floor(Convert.ToSingle(TileData.AltitudeNW + TileData.AltitudeNE + TileData.AltitudeSE + TileData.AltitudeSW) / 4)));
    }

    internal void MoveUp()
    {
        SetAltitude(TileData.Altitude + 1);
    }

    internal void MoveDown()
    {
        SetAltitude(TileData.Altitude - 1);
    }

    internal void MovePointNW(int delta)
    {
        TileData.AltitudeNW += delta;
        UpdateAltitude();
    }    

    internal void MovePointNE(int delta)
    {
        TileData.AltitudeNE += delta;
        UpdateAltitude();
    }

    internal void MovePointSW(int delta)
    {
        TileData.AltitudeSW += delta;
        UpdateAltitude();
    }

    internal void MovePointSE(int delta)
    {
        TileData.AltitudeSE += delta;
        UpdateAltitude();
    }

    public void UpdateColor()
    {
        if (transform.GetComponent<Renderer>() == null)
            return;

        switch (TileData.TileType)
        {
            case TileType.None:
                transform.GetComponent<Renderer>().material.color = new Color(0.21f, 0.16f, 0.16f);
                break;
            case TileType.Forest:
                transform.GetComponent<Renderer>().material.color = new Color(0.133f, 0.545f, 0.133f);
                break;
            case TileType.Plain:
                //transform.renderer.material.color = new Color(0.000f, 1.000f, 0.498f);
                break;
            case TileType.River:
                //transform.renderer.material.color = Color.cyan;
                break;
            case TileType.Lake:
                //transform.renderer.material.color = Color.blue;
                break;
            case TileType.City:
                transform.GetComponent<Renderer>().material.color = Color.gray;
                break;
            case TileType.Desert:
                transform.GetComponent<Renderer>().material.color = new Color(0.87f, 0.85f, 0.47f);
                break;
            case TileType.Swamp:
                transform.GetComponent<Renderer>().material.color = new Color(0.086f, 0.204f, 0.04f);
                break;
            case TileType.Fire:
                transform.GetComponent<Renderer>().material.color = new Color(0.30f, 0.22f, 0);
                break;
            case TileType.WitheredLand:
                transform.GetComponent<Renderer>().material.color = new Color(0.13f, 0.09f, 0.04f);
                break;
            case TileType.Blizzard:
                transform.GetComponent<Renderer>().material.color = new Color(0.9f, 1f, 1f);
                break;
            case TileType.FrozenLand:
                transform.GetComponent<Renderer>().material.color = new Color(0.7f, 1f, 1f);
                break;
            default:
                transform.GetComponent<Renderer>().material.color = Color.white;
                break;
        }
    }

    public void UpdateTerrainObjsPlacement()
    {
        foreach (var terrainObject in TerrainObjects)
        {
            Vector3 pos = terrainObject.transform.position;
            var hits = Physics.RaycastAll(new Vector3(pos.x, transform.position.y + GetComponent<Renderer>().bounds.size.y/2 + 3, pos.z), Vector3.down);
            foreach (var raycastHit in hits)
            {
                if (raycastHit.transform.gameObject != null)
                    terrainObject.transform.position = raycastHit.point;
            }
        }
    }

    public void SetupTerrainObjs()
    {
        TerrainObjects.Clear();
        if (TileData.TileType == TileType.Forest)
        {
            int treeCount = Random.Range(5, 7);
            PlaceProps(treeCount, forestProps, 2f);
        }
        else if (TileData.TileType == TileType.Fire)
        {
            GameObject fire = (GameObject) (Instantiate(
                Resources.Load<GameObject>("Prefabs/Effects/Fire"),
                transform.position,
                Quaternion.Euler(Vector3.zero)));
            fire.transform.parent = transform;
            TerrainObjects.Add(fire);
        }
        else if (TileData.TileType == TileType.City)
        {
            GameObject building = (GameObject) (Instantiate(
                Resources.Load<GameObject>(buildings[Random.Range(0, buildings.Length)]),
                new Vector3(transform.position.x, transform.position.y + (GetComponent<Renderer>().bounds.size.y / 2), transform.position.z),
                Quaternion.Euler(Vector3.zero)));
            building.transform.parent = transform;
            TerrainObjects.Add(building);
        }
        else if (TileData.TileType == TileType.Desert)
        {
            GameObject sand = (GameObject) (
                Instantiate(Resources.Load<GameObject>("Prefabs/Effects/Sand"),
                    Vector3.zero,
                    Quaternion.identity));
            sand.transform.SetParent(transform, false);
            TerrainObjects.Add(sand);

            int propCount = Mathf.RoundToInt(3f * Mathf.Pow(Random.Range(0, 1f), 5)); // Random distribution weighted towards lower values (source http://stackoverflow.com/a/4421297)
            PlaceProps(propCount, desertProps, .5f);
        }
        else if (TileData.TileType == TileType.FrozenLand)
        {
            if (Random.Range(0, 5) == 0)
                PlaceProps(1, frozenProps, 1f);
        }
        else if (TileData.TileType == TileType.Plain)
        {
            int propCount = Mathf.RoundToInt(3f * Mathf.Pow(Random.Range(0, 1f), 5));
            PlaceProps(propCount, plainProps, 2f);
        }
        else if (TileData.TileType == TileType.Blizzard)
        {
            GameObject snow = (GameObject) (
                Instantiate(Resources.Load<GameObject>("Prefabs/Effects/Snow"),
                    Vector3.zero,
                    Quaternion.identity));
            snow.transform.SetParent(transform, false);
            TerrainObjects.Add(snow);
        }
        else if (TileData.TileType == TileType.Swamp)
        {
            GameObject flies = (GameObject)(
                Instantiate(Resources.Load<GameObject>("Prefabs/Effects/Swamp"),
                    Vector3.zero,
                    Quaternion.identity));
            flies.transform.SetParent(transform, false);
            TerrainObjects.Add(flies);

            int propCount = Mathf.RoundToInt(3f * Mathf.Pow(Random.Range(0, 1f), 5));
            PlaceProps(propCount, swampProps, 3f);
        }
        else if (TileData.TileType == TileType.WitheredLand)
        {
            int propCount = Mathf.RoundToInt(3f * Mathf.Pow(Random.Range(0, 1f), 5));
            PlaceProps(propCount, witheredProps, 1f);
        }
    }

    private void PlaceProps(int propCount, IEnumerable<string> propPathsCollection, float minDistance)
    {
        for (int i = 0; i < propCount; i++)
        {
            Vector3 v = Vector3.zero;
            float xRangeOver2 = (GetComponent<Collider>().bounds.size.x * 0.9f) / 2;
            float zRangeOver2 = (GetComponent<Collider>().bounds.size.z * 0.9f) / 2;
            int tries = 0;
            bool keepTrying;
            do
            {
                if (keepTrying = ++tries <= propPlacementTryCount)
                {
                    v.x = Random.Range(-xRangeOver2, xRangeOver2);
                    v.z = Random.Range(-zRangeOver2, zRangeOver2);
                }
            } while (TerrainObjects.Any((o) => Vector3.Distance(o.transform.localPosition, v) < minDistance) && keepTrying);
            int index = Random.Range(0, propPathsCollection.Count());
            GameObject prop = (GameObject)(Instantiate(
                Resources.Load<GameObject>(propPathsCollection.ElementAt(index)),
                v,
                RandomYRotation()));
            prop.transform.SetParent(transform, false);
            TerrainObjects.Add(prop);
        }
    }

    private Quaternion RandomYRotation()
    {
        return Quaternion.Euler(new Vector3(0f, Random.Range(0f, 360f), 0f));
    }

    public Tile GetDataCopy()
    {
        return Tile.Copy(TileData);
    }

    public void CopyValuesFromBuffer(Tile t)
    {
        TileData.Temperature = t.Temperature;
        TileData.Humidity = t.Humidity;
        TileData.ChangeTimestamp = t.ChangeTimestamp;
    }

    public void ChangeView(MapView view)
    {
        string newShaderName = "Diffuse";

        switch (view)
        {
            case MapView.Normal:
                newShaderName = "Diffuse";
                UpdateColor();
                RenderTerrain = true;
                break;
            case MapView.Temperature:
                newShaderName = "Custom/HeatMap";
                RenderTerrain = false;
                break;
            case MapView.Humidity:
                newShaderName = "Custom/HumidityMap";
                RenderTerrain = false;
                break;
        }

        ApplyShader(newShaderName);
        UpdateMapViewValues(view);
    }

    public void UpdateMapViewValues(MapView view)
    {
        Action<string, float> setFloatHelper = (name, value) =>
        {
            if (transform.GetComponent<Renderer>() != null)
            {
                foreach (Material material in transform.GetComponent<Renderer>().materials) material.SetFloat(name, value);
            }
            else
            {
                foreach (Transform child in transform)
                {
                    foreach (Material material in child.GetComponent<Renderer>().materials) material.SetFloat(name, value);
                }
            }
        };

        switch (view)
        {
            case MapView.Temperature:
                setFloatHelper("Temperature", TileData.Temperature);
                break;
            case MapView.Humidity:
                setFloatHelper("Humidity", TileData.Humidity);
                break;
        }
    }

    public void ApplyShader(string shaderName)
    {
        if (transform.GetComponent<Renderer>() != null)
        {
            foreach (Material material in transform.GetComponent<Renderer>().materials) material.shader = Shader.Find(shaderName);
        }
        foreach (Transform child in transform)
        {
            // Should be done for all tiles with particles
            // Particles use their own shader with transparency and stuff, better to leave them alone
            if ((TileData.TileType & (TileType.Fire | TileType.Desert | TileType.Blizzard | TileType.Swamp)) == TileData.TileType) continue;
            ApplyShader(child, shaderName);
        }
    }

    //
    // TODO: move this method to some helper class
    // 
    public void ApplyShader(Transform t, string shaderName)
    {
        if (t.GetComponent<Renderer>() != null)
        {
            foreach (Material material in t.GetComponent<Renderer>().materials) material.shader = Shader.Find(shaderName);
        }

        foreach (Transform child in t)
        {
            ApplyShader(child, shaderName);
        }
    }

    private void Start()
    {
        UpdateColor();
    }

    private void Update()
    {

    }
    public override string ToString()
    {
        return TileData.TileType.ToString();
    }
}
