﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

public class SelectMapMenu : MonoBehaviour
{
    public GameObject SelectMapVerticalLayout;
    public GameObject SelectMapButtonPrefab;
    public Scrollbar SelectMapScrollBar;

    private AudioControl Audio;

    void Awake()
    {
        Audio = AudioControl.AudioInCurrentScene;

        var mapFiles = Resources.LoadAll<TextAsset>("Maps");
        foreach (var map in mapFiles)
        {
            var tmpName = map.name.Clone().ToString();
            if (Globals.MapNames.Contains(tmpName))
            {
                GameObject button = (GameObject)Instantiate(SelectMapButtonPrefab, Vector3.zero, Quaternion.identity);
                button.GetComponent<Button>().onClick.AddListener(() => Play(tmpName));
                Text t = (Text)(button.transform.GetChild(0).GetComponent<Text>());
                t.text = map.name;
                button.transform.SetParent(SelectMapVerticalLayout.transform, false);
            }
        }

        SelectMapVerticalLayout.GetComponent<RectTransform>().sizeDelta = new Vector2(SelectMapVerticalLayout.GetComponent<RectTransform>().sizeDelta.x, Globals.MapNames.Length * 95);
        SelectMapScrollBar.value = 0.99f;
    }

    void Start()
    {

    }

    void Update()
    {

    }

    public void Play(string mapName)
    {
        PlayerPrefs.SetString("CurrentLevel", mapName);
        Application.LoadLevel("Game");
        Audio.PlayMenuClickIntense();
    }
}
